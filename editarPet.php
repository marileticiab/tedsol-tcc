<?php
    include_once("conexao.php");

    if(isset($_POST["nome"])){
       
        $idP = $_GET["id"];
        $nome = $_POST["nome"];
        $peso = $_POST["peso"];
        $raca = $_POST["raca"];
        $datanascimento = $_POST["datanasc_pet"];
        $genero = $_POST["genero"];
        $cor = $_POST["cor"];
        $especie = $_POST["especie"];
        $cpf = $_POST["cpf"];

        $sql_update = "UPDATE pet 
        SET nome = '$nome',
        peso = '$peso',
        raca = '$raca',
        datanasc = '$datanascimento',
        genero = '$genero',
        cor = '$cor',
        especie = '$especie'
        WHERE id = '$idP' ";

        if($conn->query($sql_update) === TRUE){?>
            <script>
                alert("Atualizado com sucesso!");
                history.go(-2);
            </script>
            <?php
        }else{?>
            <script>
                alert("Erro ao atualizar.");
                window.history.back();
            </script>
            <?php
        }
    }
?>

<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
  <link href="img/logotipo2.png" rel="icon">
  
    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Editar - TedSol</title>

    <!-- Icons font CSS-->
    <link href="assets/vendor-cadastro/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="assets/vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h1 class="title">Editar Pet</h1>

                    <?php
                    if(isset($_GET["id"])){
                        $id_pet = $_GET["id"];
                        $sql = "SELECT * FROM pet WHERE id = '$id_pet'";
                        $consulta = $conn->query($sql);
                        $pet = $consulta->fetch_assoc();
                    }else{
                        echo "usuario não encontrado";
                    }
                    ?>

                    <form method="POST" action="editarPet.php?id=<?php echo $_GET['id'] ?>">
                        <h4>DADOS</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nome</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="nome" value="<?php echo $pet["nome"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Peso</label>
                                    <input required placeholder = "*" class="input--style-4" type="number" name="peso" value="<?php echo $pet["peso"] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Raça</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="raca" value="<?php echo $pet["raca"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Data de nascimento</label>
                                    <div class="input-group-icon">
                                        <input required placeholder = "*" class="input--style-4 js-datepicker" type="date" name="datanasc_pet" value="<?php echo $pet["datanasc"] ?>">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Gênero</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="genero" value="<?php echo $pet["genero"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Cor</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="cor" value="<?php echo $pet["cor"] ?>">
                                </div>
                            </div>
                            
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Espécie</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="especie" value="<?php echo $pet["especie"] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="p-t-15">
                           
                            <button class="btn btn--radius-2 btn--blue" type="submit">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
       
        function mascara(i,t){

            var v = i.value;

            if(isNaN(v[v.length-1])){
                i.value = v.substring(0, v.length-1);
                return;
            }

            if(t == "cpf"){
                i.setAttribute("maxlength", "14");
            if (v.length == 3 || v.length == 7) i.value += ".";
            if (v.length == 11) i.value += "-";
            }
            if(t == "tel"){
                if(v[0] == 9){
                     i.setAttribute("maxlength", "10");
                    if (v.length == 5) i.value += "-";
                    }else{
                    i.setAttribute("maxlength", "9");
                if (v.length == 4) i.value += "-";
                    }

            }
        }
    </script>
    <!-- Jquery JS-->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <!-- Vendor JS-->
    <!--<script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>-->

    <!-- Main JS-->
    <!--<script src="js/global.js"></script>-->

</body>

</html>