<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
  <link href="img/logotipo2.png" rel="icon">
  
    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Cadastro - TedSol</title>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <!-- Icons font CSS-->
    <link href="assets/vendor-cadastro/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="assets/vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h1 class="title">Cadastro</h1>
                    <form method="POST" action="inserirVet.php" enctype="multipart/form-data">
                        <h4>DADOS PESSOAIS</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nome</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="nome" id="nome">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Sobrenome</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="sobrenome" id="sobrenome">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">CPF</label>
                                    <input required placeholder = "*" oninput="mascara(this,'cpf')" class="input--style-4" type="text" name="cpf" id="cpf"placeholder="Ex.: xxx.xxx.xxx-xx" autocomplete="off" >
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Data de nascimento</label>
                                    <div class="input-group-icon">
                                        <input required placeholder = "*" class="input--style-4 js-datepicker" type="date" name="datanascimento" id="datanascimento">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <h4>CONTATO</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input required placeholder = "*" class="input--style-4" type="email" name="email" id="email">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Telefone</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="telefone" id="telefone" maxlength="15" onkeyup="handlePhone(event)" placeholder="Ex.:(xx)xxxxx-xxxx" autocomplete="off"> 

                                </div>
                            </div>
                        </div>

                        <h4>ENDEREÇO</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="cep" class="label">CEP</label>
                                    <input required placeholder = "*" oninput="mascaraCep(this,'cep')" class="input--style-4" type="text" name="cep" id="cep">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="complemento" class="label">Complemento</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="complemento" id="complemento">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="rua" class="label">Rua</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="rua" id="rua">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="numero" class="label">N°</label>
                                    <input required placeholder = "*" class="input--style-4" type="number" name="numero" id="numero">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="bairro" class="label">Bairro</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="bairro" id="bairro">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="cidade" class="label">Cidade</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="cidade" id="cidade">
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="form-group col-md-4">
                                <label for="estado" class="label">Estado</label>
                                <input required placeholder = "*" class="input--style-4" type="text" name="estado" id="uf">
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="pais" class="label">País</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="pais" id="pais">
                                </div>
                            </div>
                        </div>
                       
                        <h4>IDENTIFICAÇÃO</h4>
                        <br>
                        
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Formação</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="formacao" id="formacao">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Faculdade de Graduação</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="faculdade" id="faculdade">
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Crmv</label>
                                    <input required placeholder = "*" class="input--style-4" type="number" name="crmv" id="crmv">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Senha</label>
                                    <input required placeholder = "*" class="input--style-4" type="password" name="senha" id="password">
                                    <!--<span  class="password-toggle" id="toggle-password"  onclick="mostrarOcultarSenha()">Mostrar</span>-->
                                </div>
                            </div>
                        </div>

                        <br>
                        
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Qual o seu tipo de usuário?</label>
                                        <div required placeholder = "*" class="radio-container">
                                            <label class="radio-label">
                                                <input type="radio" name="opcao" class="radio-input" value="1">
                                                Clínica
                                            </label>
                                            <label class="radio-label">
                                                <input type="radio" name="opcao" class="radio-input" value="2">
                                                Veterinário
                                            </label>
                                        </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <label class="label">Foto de Perfil</label>
                                <label for="file">Selecione o arquivo</label>
                                <input type="file" name="file" id="file" required>
                                <br>
                            </div>
                        </div>

                        <br>

                        <div class="p-t-15">
                            <button class="btn btn--radius-2 btn--blue" type="submit" value="Enviar" name="bntCadastrar">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function mostrarOcultarSenha(){
            const passwordInput = document.getElementById("password");
            const toggleButton = document.getElementById("toggle-password");

            toggleButton.addEventListener("click", function() {
            if (passwordInput.type === "password") {
                passwordInput.type = "text";
                toggleButton.textContent = "Ocultar";
            } else {
                passwordInput.type = "password";
                toggleButton.textContent = "Mostrar";
            }
            });
        }

        function mascaraCep(cep){
            if (cep.value.length == 5) {
                cep.value = cep.value + '-' 
            }
        }

        function mascara(i,t){

            var v = i.value;

            if(isNaN(v[v.length-1])){
                i.value = v.substring(0, v.length-1);
                return;
            }

            if(t == "cpf"){
                i.setAttribute("maxlength", "14");
            if (v.length == 3 || v.length == 7) i.value += ".";
            if (v.length == 11) i.value += "-";
            }
        }
        
        const handlePhone = (event) => {
        let input = event.target
        input.value = phoneMask(input.value)
        }

        const phoneMask = (value) => {
        if (!value) return ""
        value = value.replace(/\D/g,'')
        value = value.replace(/(\d{2})(\d)/,"($1) $2")
        value = value.replace(/(\d)(\d{4})$/,"$1-$2")
        return value
        }

        (function(){

        const cep = document.querySelector("input[name=cep]");

        cep.addEventListener('blur', e=> {
            const value = cep.value.replace(/[^0-9]+/, '');
        const url = `https://viacep.com.br/ws/${value}/json/`;
        
        fetch(url)
        .then( response => response.json())
        .then( json => {
                
            if( json.logradouro ) {
                document.querySelector('input[name=rua]').value = json.logradouro;
                document.querySelector('input[name=bairro]').value = json.bairro;
                document.querySelector('input[name=cidade]').value = json.localidade;
                document.querySelector('input[name=estado]').value = json.uf;
            }
        
        });
        
        });

        })();
    </script>

</body>

</html>