<?php

if(isset($_POST["upload"])){

    $file = $_FILES["file"];

    $folder = "uploads";

    $permite = array("tif", "jpg", "png", "pdf", "jfif");
    $maxSize = 1024 * 1024 * 5;

    $msg = array();
    $erroMsg = array( 1 => "O arquivo é maior do que o permitido." ,
                        2 => "Não foi possível fazer o upload.");
    
    $name = $file["name"];
    $type = $file["type"];    
    $size = $file["size"];  
    $error = $file["error"];  
    $tmp = $file["tmp_name"]; 
    
    $extensao = @end(explode(".", $name));  
  
    $novoNome = rand() . ".$extensao";

    if($error != 0){
        $msg[] = "<b>$name</b>" . $erroMsg[$error];

    }else if(!in_array($extensao, $permite)){
        $msg[] = "<b>$name</b>" . "Tipo de arquivo não suportado.";
    
    }else if($size > $maxSize){
        $msg[] = "<b>$name</b>" . "Tamanho maior que o permitido.";
    
    }else{
        if(move_uploaded_file($tmp, $folder."/".$novoNome)){

           $msg[] = "Arquivo aceito com sucesso!";
           //header("Location: editarImg.php?nomeImg=novoNome");
           header("Location: inserirVet.php?nome=$novoNome");
            exit();

        }else{
            $msg[] = "Ocorreu algum erro ao fazer o upload.";
        }
    }

    foreach($msg as $value){
        echo $value . "<br>";
    }
    //header("Location: perfilVet.php");
}

?>