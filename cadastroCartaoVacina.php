<?php
include_once('conexao.php');
session_start();
?>

<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
  <link href="img/logotipo2.png" rel="icon">
  
    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Cadastro - TedSol</title>

    <!-- Icons font CSS-->
    <link href="assets/vendor-cadastro/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="assets/vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
<?php
    //iniciando sessão
    if(isset($_SESSION["usuario"])){
        $idPet = $_GET["id"];
        $sqlPet = "SELECT id FROM pet WHERE id = $idPet";

        $consultaPet = $conn->query($sqlPet);
        $idPet = $consultaPet->fetch_assoc();
?>

    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h1 class="title">Cadastro Vacina / Vermífugo</h1>
                    <form method="POST" action="inserirVacina.php?id=<?php echo $idPet['id'];?>">
                        <h4>INFORMAÇÕES</h4>
                        <br>
                        <br>
                        <div class="row row-space">
                            <div class="radio-container">
                                <label class="radio-label">
                                    <input type="radio" name="opcao" class="radio-input" value="1">
                                    Vacina
                                </label>
                                <label class="radio-label">
                                    <input type="radio" name="opcao" class="radio-input" value="2">
                                    Vermífugo
                                </label>
                            </div>

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Vacina/Vermífugo</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="nome">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Data</label>
                                    <div class="input-group-icon">
                                        <input required placeholder = "*" class="input--style-4 js-datepicker" type="date" name="data">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Data de Reforço</label>
                                    <div class="input-group-icon">
                                        <input required placeholder = "*" class="input--style-4 js-datepicker" type="date" name="data_reforco">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="p-t-15">
                           
                            <button class="btn btn--radius-2 btn--blue" type="submit">Adicionar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
        }else{
            echo "Usuário não autenticado.";
        }
    ?>
</body>

</html>