<?php
include_once("conexao.php");
session_start();

$pesquisa = $_POST["pesquisa"];

    //iniciando sessão
    if(isset($_SESSION["id"])){
        //session_start();
                                        //$id_final = $exibir_cliente['id'];
                        //echo $exibir_cliente['nome'];
                        $sql_2 = "SELECT *
                        FROM pet 
                        WHERE nome like '%$pesquisa%'  
                        ORDER BY nome";

                        //echo $sql_2;

                        $dados_pet = $conn->query($sql_2);

                        if($dados_pet->num_rows > 0){
?>

<table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Data Nascimento</th>
                        <th>Raça</th>
                        <th>Peso</th>
                        <th>Editar</th>
                        <th>Excluir</th>
                        <th>Detalhes</th>
                    </tr>
                </thead>

                <?php
                    while($exibir = $dados_pet->fetch_assoc()){
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'];?></td>
                        <td><?php echo $exibir['datanasc'];?></td>
                        <td><?php echo $exibir['raca'] ;?></td>
                        <td><?php echo $exibir['peso'] ;?> Kg</td>
                        <td><a href="editarPet.php?id=<?php echo $exibir['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a style = "font-color: blue" onclick="confirmaExclusao('<?php echo $exibir['id'];?>')"><i class="bi bi-trash3"></i></a></td>
                        <td><a  class="bnt-add" href="perfilPet.php?id=<?php echo $exibir['id'];?>"><i class="bi bi-three-dots"></i></a></td>
                    </tr>
                </tbody>

                <?php
                    }
                }else{
                    echo "Não há pets cadastrados.";
                }
            }
                ?>

            </table>