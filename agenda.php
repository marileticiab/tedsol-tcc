<?php
session_start();
include('conexao.php');
?>

<!doctype html>
<html lang="pt">

<head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!--<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>-->

    <!--CSS-->
    <link rel="stylesheet" href="css/style-agenda.css">

    <style>
        @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

</head>

<body className='snippet-body'>

    <?php
        if(isset($_SESSION["usuario"])){
            $tipo_user = $_SESSION["tipo_usuario"];
            $vet_id = $_SESSION["id"];
            if($tipo_user == 1 || $tipo_user == 2){
                require_once("menuVet.php");
            }else if($tipo_user == 3){
                require_once("menuCliente.php");
           }
    ?>

    <section class="ftco-section">
        <!--Container Main start-->
           
        
        <?php
            $sql = "SELECT * FROM horario_funcionamento;";
            $sql_query = $conn->query($sql);

            $quantidade = $sql_query->num_rows;

            if($quantidade > 0){
                $horario = $sql_query->fetch_assoc();
            }
        ?>
                <div class="container2">
                    <div class="row justify-content-center">
                        <div class="col-md-6 text-center mb-5">
                            <h1 class="title-model">Agenda</h2>
                            <br>
                            <h6>Horários de Funcionamento:</h6>
                            <div class="hora-agenda">
                                <label style= "font-weight: bold">ABERTO: <?php echo $horario["aberto"] ?></label>
                                <br>
                                <label style= "font-weight: bold">FECHADO: <?php echo $horario["fechado"] ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content w-100" style="background-color: #8fb9aa;">
                            <div class="calendar-container">
                              <div class="calendar"> 
                                <div class="year-header"> 
                                  <span class="left-button fa fa-chevron-left" id="prev"> </span> 
                                  <span class="year" id="label"></span> 
                                  <span class="right-button fa fa-chevron-right" id="next"> </span>
                                </div> 
                                <table class="months-table w-100"> 
                                  <tbody>
                                    <tr class="months-row">
                                      <td class="month">Jan</td> 
                                      <td class="month">Fev</td> 
                                      <td class="month">Mar</td> 
                                      <td class="month">Abr</td> 
                                      <td class="month">Mai</td> 
                                      <td class="month">Jun</td> 
                                      <td class="month">Jul</td>
                                      <td class="month">Ago</td> 
                                      <td class="month">Set</td> 
                                      <td class="month">Out</td>          
                                      <td class="month">Nov</td>
                                      <td class="month">Dez</td>
                                    </tr>
                                  </tbody>
                                </table> 
                                
                                <table class="days-table w-100"> 
                                  <td class="day">Dom</td> 
                                  <td class="day">Seg</td> 
                                  <td class="day">Ter</td> 
                                  <td class="day">Qua</td> 
                                  <td class="day">Qui</td> 
                                  <td class="day">Sex</td> 
                                  <td class="day">Sab</td>
                                </table> 
                                <div class="frame"> 
                                  <table class="dates-table w-100"> 
                                  <tbody class="tbody">             
                                  </tbody> 
                                  </table>
                                </div> 
                                <button class="button" id="add-button">Agendar</button>
                              </div>
                            </div>
                            <div class="events-container" style="background-color:#8fb9aa;">
                            <!--MEXER AQUI-->
                            </div>
                            <div class="dialog" id="dialog">
                                <h2 class="dialog-header"> Agendar </h2>
                                <form class="form" id="form" method="POST" action="inserirAgendamento.php">
                                    
                                  <div class="form-container" align="center">

                                    <label class="form-label" id="valueFromMyButton" for="vet">Veterinário</label>
                                    <br>
                                        <select name="vet">

                                            <?php

                                            if($tipo_user == 1 || $tipo_user == 3){
                                                
                                                $sql_vet = "SELECT id, nome, sobrenome, crmv FROM veterinario ORDER BY nome";

                                                $vet = $conn->query($sql_vet);

                                                while($row_vet = $vet->fetch_assoc()){
                                                    ?>
                                                        <option required value = "<?php echo $row_vet["id"]; ?>"> <?php echo $row_vet["nome"] ." ". $row_vet["sobrenome"] ." | CRMV:". $row_vet["crmv"] ?> </option>
                                                    <?php
                                                }

                                            }else if($tipo_user == 2){
                                                ?>
                                                    <option required value = "<?php echo $_SESSION["id"]; ?>"> <?php echo $_SESSION["nome"] ." ". $_SESSION["sobrenome"] ." | CRMV:". $_SESSION["crmv"]?> </option>
                                                <?php
                                                }
                                            ?>
                                        </select>
                                                <br>
                                    <label class="form-label" id="valueFromMyButton" for="namePet">Pet</label>
                                    <br>
                                        <select name="pet">

                                            <?php
                                            if($tipo_user == 1 || $tipo_user == 2){
                                                include_once('conexao.php');
                                                $sql_pet = "SELECT id, nome, id_cliente FROM pet ORDER BY nome";

                                                $pet = $conn->query($sql_pet);

                                                while($row_pet = $pet->fetch_assoc()){
                                                    $idC = $row_pet["id_cliente"];
                                                    $sql_cliente = "SELECT nome, sobrenome FROM clientes WHERE id = '$idC'";
                                                    $cliente = $conn->query($sql_cliente);
                                                    $row_cliente = $cliente->fetch_assoc();
                                                    ?>
                                                        <option required value= "<?php echo $row_pet["id"]; ?>"> <?php echo $row_pet["nome"] . " | Tutor:". $row_cliente["nome"] . " " . $row_cliente["sobrenome"]?> </option>
                                                    <?php
                                                }
                                            }else if($tipo_user == 3){

                                                include_once('conexao.php');
                                                $cliente =  $_SESSION['id'];
                                                $sql_pet = "SELECT id, nome FROM pet WHERE id_cliente = '$cliente' ORDER BY nome";

                                                $pet = $conn->query($sql_pet);

                                                while($row_pet = $pet->fetch_assoc()){
                                                    ?>
                                                        <option required value= "<?php echo $row_pet["id"]; ?>"> <?php echo $row_pet["nome"]?> </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        
                                        </select>
                                    <br>
                                    <label class="form-label" id="valueFromMyButton" for="tipo">Tipo</label><br>
                                        <select name="tipoAgenda">

                                            <?php
                                                include_once('conexao.php');
                                                $sql = "SELECT id, tipo FROM tipo_agendamento ORDER BY id";

                                                $tipo = $conn->query($sql);

                                                while($rowTipo = $tipo->fetch_assoc()){
                                                    ?>
                                                        <option required value= "<?php echo $rowTipo["id"]; ?>"> <?php echo $rowTipo["tipo"]; ?> </option>
                                                    <?php
                                                }
                                            ?>
                                        </select>

                                        <br>

                                        <label class="form-label" id="valueFromMyButton" for="data">Data</label>
                                        <input name="data" class="input" type="date" min="" id="data" required>

                                        <label class="form-label" id="valueFromMyButton" for="horario">Horário</label>
                                        <input name="horario" placeholder="HH:mm:ss" pattern="\d{2}:\d{2}:\d{2}" class="input" step="1800" type="time" min="<?php echo $horario['aberto'];?>" max="<?php echo $horario['fechado'];?>" require id="horario" required >

                                        <button  class="button button-white" id="ok-button" type="submit" value="OK" name="bntCadastrar">OK</button>
                                        <input type="button" value="Cancelar" class="button" id="cancel-button">
                                        
                                  </div>
                                </form>
                              </div>
                          </div>  
                        </div>
                    </div>
                </div>
                <br><br>
                <!--consultas marcadas-->
                <div class="container-table">
                <?php 
                if($tipo_user != 3 ){?>
                <h1 class="title-model">Ações</h1>

                    <?php 
                        date_default_timezone_set('America/Sao_Paulo');
                        $currentDateAndTime = new DateTime();
                        $day = $currentDateAndTime->format('d');
                        $month = $currentDateAndTime->format('m');
                        $year = $currentDateAndTime->format('Y');
                        $hours = $currentDateAndTime->format('H');
                        $minutes = $currentDateAndTime->format('i');
                        $seconds = $currentDateAndTime->format('s');
                        $dataatual=$currentDateAndTime->format('d-m-Y');
                        
                    ?>
                    <br><br>

            <!--HOJE -------------------------------------------------------------------------------------------------------->
                <h2 class="title-model">Horarios de Hoje: <?php echo $dataatual?></h2>
                <?php
                
                $dataatual=$currentDateAndTime->format('Y-m-d');

                if($tipo_user == 1){
                    $sql = "SELECT * FROM agendamento WHERE dia = '" .$dataatual. "' ORDER BY dia";
                    $dados_agendamento = $conn->query($sql);

                    if($dados_agendamento->num_rows > 0){
                   
                ?>
                    <table class="styled-table">
                        <thead>
                            <th colspan = "2">Notificar</th>
                            <th>Horário</th>
                            <th>Tipo</th>
                            <th>Vet</th>
                            <th>Pet</th>
                            <th>Situação</th>
                        </thead>
                        <?php
                        while($exibir = $dados_agendamento->fetch_assoc()){
                    ?>

                    <tbody>
                        <tr>
                            <?php
                                $notificacao = $exibir['notificado'];
                                $sqlnotificacao= "SELECT * FROM notificacao WHERE id = '$notificacao'";
                                $dadosnotificacao= $conn->query($sqlnotificacao);
                                $notificacao= $dadosnotificacao->fetch_assoc();
                            ?>
                            <td <?php if($exibir['notificado']==1){?>
                            style = "color: green"
                            <?php }else{ ?>
                            style = "color: red"
                            <?php } ?>   
                            ><?php echo $notificacao['notificacao'];?></td>
                            <td><a class="bnt-add" onclick="confirmaNotificacao('<?php echo $exibir['id'];?>')">
                            <i class="bi bi-bell-fill"></i></a>
                            </td>

                            <td><?php echo $exibir['horario'];?></td>
                            <?php
                                $tipo= $exibir['tipo'];
                                $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                                $dadostipo= $conn->query($sqltipo);
                                $nometipo= $dadostipo->fetch_assoc();
                            ?>
                            <td><?php echo $nometipo['tipo'];?></td>
                            <?php
                                $vet = $exibir['id_veterinario'];
                                $sql_idvet = "SELECT nome, sobrenome, crmv FROM veterinario WHERE id = '$vet'";
                                $dadosidvet= $conn->query($sql_idvet);
                                $vet_agenda= $dadosidvet->fetch_assoc();
                            ?>
                            <td><?php echo $vet_agenda['nome'] . " " . $vet_agenda['sobrenome'] . " | CRMV: " . $vet_agenda['crmv'];?></td>
                            <?php
                                $pet= $exibir['id_pet'];
                                $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                                $dadospet= $conn->query($sqlpet);
                                $nomepet= $dadospet->fetch_assoc();
                            ?>
                            <td><?php echo $nomepet['nome'];?></td>

                            <?php
                                $situacao = $exibir['situacao'];
                                $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                                $dados_situacao= $conn->query($sql_situacao);
                                $situacao_resultado= $dados_situacao->fetch_assoc();
                            ?>
                            <td <?php if($exibir['situacao']==1){?>
                                style = "color: green"
                                <?php }else if($exibir['situacao']==2){?>
                                style = "color: orange"
                                <?php }else{?>
                                style = "color: red" 
                                <?php } ?>
                                ><?php echo $situacao_resultado['situacao'];?></td>
                    </tbody>

                    <?php
                        }
                        }else{
                            ?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados para hoje.</h5>
                            <?php
                        }
                    ?>
                    </table>

                    <?php
                    }else if($tipo_user == 2){
                        $sql = "SELECT * FROM agendamento WHERE id_veterinario = '$vet_id' AND  dia = '" .$dataatual. "' ORDER BY dia";
                        $dados_agendamento = $conn->query($sql);

                        if($dados_agendamento->num_rows > 0){
                    
                    ?>
                        <table class="styled-table">
                            <thead>
                                <th colspan = "2">Notificar</th>
                                <th>Horário</th>
                                <th>Tipo</th>
                                <th>Pet</th>
                                <th>Situação</th>
                            </thead>
                            <?php
                            while($exibir = $dados_agendamento->fetch_assoc()){
                        ?>

                        <tbody>
                            <tr>
                                <?php
                                    $notificacao = $exibir['notificado'];
                                    $sqlnotificacao= "SELECT * FROM notificacao WHERE id = '$notificacao'";
                                    $dadosnotificacao= $conn->query($sqlnotificacao);
                                    $notificacao= $dadosnotificacao->fetch_assoc();
                                ?>
                                <td <?php if($exibir['notificado']==1){?>
                                style = "color: green"
                                <?php }else{ ?>
                                style = "color: red"
                                <?php } ?>   
                                ><?php echo $notificacao['notificacao'];?></td>
                                
                                <td><a class="bnt-add" onclick="confirmaNotificacao('<?php echo $exibir['id'];?>')">
                                <i class="bi bi-bell-fill"></i></a>
                                </td>

                                <td><?php echo $exibir['horario'];?></td>
                                <?php
                                    $tipo= $exibir['tipo'];
                                    $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                                    $dadostipo= $conn->query($sqltipo);
                                    $nometipo= $dadostipo->fetch_assoc();
                                ?>
                                <td><?php echo $nometipo['tipo'];?></td>
                                
                                <?php
                                    $pet= $exibir['id_pet'];
                                    $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                                    $dadospet= $conn->query($sqlpet);
                                    $nomepet= $dadospet->fetch_assoc();
                                ?>
                                <td><?php echo $nomepet['nome'];?></td>

                                <?php
                                    $situacao = $exibir['situacao'];
                                    $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                                    $dados_situacao= $conn->query($sql_situacao);
                                    $situacao_resultado= $dados_situacao->fetch_assoc();
                                ?>
                                <td <?php if($exibir['situacao']==1){?>
                                    style = "color: green"
                                    <?php }else if($exibir['situacao']==2){?>
                                    style = "color: orange"
                                    <?php }else{?>
                                    style = "color: red" 
                                    <?php } ?>
                                    ><?php echo $situacao_resultado['situacao'];?></td>
                        </tbody>

                        <?php
                            }
                            }else{
                                ?>
                                <h5 style = "padding-left: 50px">Não há agendamentos cadastrados para hoje.</h5>
                                <?php
                            }
                        ?>
                        </table>

                        <?PHP
                        }else{
                            ?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados.</h5>
                            <?php
                        }
                        ?>
                        
                        <!--here-->
                        <br><br>

              <!--AMANHÃ---------------------------------------------------------------------------------------------->    
                <?php
                     $amanha = date('d-m-Y', strtotime($dataatual . ' +1 day'));
                     
                ?>   
                <h2 class="title-model">Horarios de Amanhã: <?php echo $amanha?></h2>
                <?php
                
                $amanha = date('Y-m-d', strtotime($dataatual . ' +1 day'));

                if($tipo_user == 1){
                    $sql = "SELECT * FROM agendamento WHERE dia = '" .$amanha. "' ORDER BY dia";
                    $dados_agendamento = $conn->query($sql);

                    if($dados_agendamento->num_rows > 0){
                   
                ?>
                    <table class="styled-table">
                        <thead>
                            <th colspan = "2">Notificar</th>
                            <th>Horário</th>
                            <th>Tipo</th>
                            <th>Vet</th>
                            <th>Pet</th>
                            <th>Situação</th>
                        </thead>
                        <?php
                        while($exibir = $dados_agendamento->fetch_assoc()){
                    ?>

                    <tbody>
                        <tr>
                            <?php
                                $notificacao = $exibir['notificado'];
                                $sqlnotificacao= "SELECT * FROM notificacao WHERE id = '$notificacao'";
                                $dadosnotificacao= $conn->query($sqlnotificacao);
                                $notificacao= $dadosnotificacao->fetch_assoc();
                            ?>
                            <td <?php if($exibir['notificado']==1){?>
                            style = "color: green"
                            <?php }else{ ?>
                            style = "color: red"
                            <?php } ?>   
                            ><?php echo $notificacao['notificacao'];?></td>
                            <td><a class="bnt-add" onclick="confirmaNotificacao('<?php echo $exibir['id'];?>')">
                            <i class="bi bi-bell-fill"></i></a>
                            </td>

                            <td><?php echo $exibir['horario'];?></td>
                            <?php
                                $tipo= $exibir['tipo'];
                                $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                                $dadostipo= $conn->query($sqltipo);
                                $nometipo= $dadostipo->fetch_assoc();
                            ?>
                            <td><?php echo $nometipo['tipo'];?></td>
                            <?php
                                $vet = $exibir['id_veterinario'];
                                $sql_idvet = "SELECT nome, sobrenome, crmv FROM veterinario WHERE id = '$vet'";
                                $dadosidvet= $conn->query($sql_idvet);
                                $vet_agenda= $dadosidvet->fetch_assoc();
                            ?>
                            <td><?php echo $vet_agenda['nome'] . " " . $vet_agenda['sobrenome'] . " | CRMV: " . $vet_agenda['crmv'];?></td>
                            <?php
                                $pet= $exibir['id_pet'];
                                $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                                $dadospet= $conn->query($sqlpet);
                                $nomepet= $dadospet->fetch_assoc();
                            ?>
                            <td><?php echo $nomepet['nome'];?></td>

                            <?php
                                $situacao = $exibir['situacao'];
                                $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                                $dados_situacao= $conn->query($sql_situacao);
                                $situacao_resultado= $dados_situacao->fetch_assoc();
                            ?>
                            <td <?php if($exibir['situacao']==1){?>
                                style = "color: green"
                                <?php }else if($exibir['situacao']==2){?>
                                style = "color: orange"
                                <?php }else{?>
                                style = "color: red" 
                                <?php } ?>
                                ><?php echo $situacao_resultado['situacao'];?></td>
                    </tbody>

                    <?php
                        }
                        }else{
                            ?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados para amanhã.</h5>
                            <?php
                        }
                    ?>
                    </table>
                    <?php
                    $amanha = date('Y-m-d', strtotime($dataatual . ' +1 day'));

                        }else if($tipo_user == 2){
                            $sql = "SELECT * FROM agendamento WHERE id_veterinario = '$vet_id' AND dia = '" .$amanha. "' ORDER BY dia";
                            $dados_agendamento = $conn->query($sql);

                            if($dados_agendamento->num_rows > 0){
                                ?>

                    <table class="styled-table">
                        <thead>
                            <th colspan = "2">Notificar</th>
                            <th>Horário</th>
                            <th>Tipo</th>
                            <th>Pet</th>
                            <th>Situação</th>
                        </thead>
                        <?php
                        while($exibir = $dados_agendamento->fetch_assoc()){
                    ?>

                    <tbody>
                        <tr>
                            <?php
                                $notificacao = $exibir['notificado'];
                                $sqlnotificacao= "SELECT * FROM notificacao WHERE id = '$notificacao'";
                                $dadosnotificacao= $conn->query($sqlnotificacao);
                                $notificacao= $dadosnotificacao->fetch_assoc();
                            ?>
                            <td <?php if($exibir['notificado']==1){?>
                            style = "color: green"
                            <?php }else{ ?>
                            style = "color: red"
                            <?php } ?>   
                            ><?php echo $notificacao['notificacao'];?></td>
                            <td><a class="bnt-add" onclick="confirmaNotificacao('<?php echo $exibir['id'];?>')">
                            <i class="bi bi-bell-fill"></i></a>
                            </td>

                            <td><?php echo $exibir['horario'];?></td>
                            <?php
                                $tipo= $exibir['tipo'];
                                $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                                $dadostipo= $conn->query($sqltipo);
                                $nometipo= $dadostipo->fetch_assoc();
                            ?>
                            <td><?php echo $nometipo['tipo'];?></td>
                            
                            <?php
                                $pet= $exibir['id_pet'];
                                $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                                $dadospet= $conn->query($sqlpet);
                                $nomepet= $dadospet->fetch_assoc();
                            ?>
                            <td><?php echo $nomepet['nome'];?></td>

                            <?php
                                $situacao = $exibir['situacao'];
                                $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                                $dados_situacao= $conn->query($sql_situacao);
                                $situacao_resultado= $dados_situacao->fetch_assoc();
                            ?>
                            <td <?php if($exibir['situacao']==1){?>
                                style = "color: green"
                                <?php }else if($exibir['situacao']==2){?>
                                style = "color: orange"
                                <?php }else{?>
                                style = "color: red" 
                                <?php } ?>
                                ><?php echo $situacao_resultado['situacao'];?></td>
                    </tbody>

                    <?php
                        }
                        }else{
                            ?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados para amanhã.</h5>
                            <?php
                        }
                    ?>
                    </table>

                        <?php
                        }else{?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados.</h5>
                            <?php
                        }?>
                   
                <?php
            }
                ?>

            <br><br>
            <h2 class="title-model">Todos</h2>
                <br>
                <?php

                if($tipo_user == 2){
                    $sql = "SELECT * FROM agendamento WHERE id_veterinario  = '$vet_id' ORDER BY dia";
                    $dados_agendamento = $conn->query($sql);

                    if($dados_agendamento->num_rows > 0){
                    ?>
                    
                    <table class="styled-table">
                        <thead>
                            <th>Dia</th>
                            <th>Horário</th>
                            <th>Tipo</th>
                            <th>Pet</th>
                            <th>Situação</th>
                            <th>Realizado</th> 
                            <th>Cancelar</th>
                        </thead>
                        <?php
                        while($exibir = $dados_agendamento->fetch_assoc()){
                    ?>

                    <tbody>
                        <tr>
                            <td><?php echo $exibir['dia'] ;?></td>
                            <td><?php echo $exibir['horario'];?></td>
                            <?php
                                $tipo= $exibir['tipo'];
                                $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                                $dadostipo= $conn->query($sqltipo);
                                $nometipo= $dadostipo->fetch_assoc();
                            ?>
                            <td><?php echo $nometipo['tipo'];?></td>
                            <?php
                                $pet= $exibir['id_pet'];
                                $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                                $dadospet= $conn->query($sqlpet);
                                $nomepet= $dadospet->fetch_assoc();
                            ?>
                            <td><?php echo $nomepet['nome'];?></td>

                            <?php
                                $situacao = $exibir['situacao'];
                                $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                                $dados_situacao= $conn->query($sql_situacao);
                                $situacao_resultado= $dados_situacao->fetch_assoc();
                            ?>
                            <td <?php if($exibir['situacao']==1){?>
                                style = "color: green"
                                <?php }else if($exibir['situacao']==2){?>
                                style = "color: orange"
                                <?php }else{?>
                                style = "color: red" 
                                <?php } ?>
                                ><?php echo $situacao_resultado['situacao'];?></td>
                            
                            <td>
                            <form method="POST" action="editarAgendamento.php?id=<?php echo $exibir['id'] ?>">
                                <input type="checkbox" id="myCheckbox">
                                <label for="myCheckbox">Marcar como realizado</label><br>
                                <button class="bnt-add" onclick="checkInput('myCheckbox')">OK</button>
                            </form>
                            </td>
                            
                            <td><a class="bnt-add" onclick="confirmaCancelamento('<?php echo $exibir['id'];?>')"><i class="bi bi-calendar2-x-fill"></i></a></td>
                            </tr>
                    </tbody>

                    <?php
                        }
                        }else{
                            ?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados.</h5>
                            <?php
                        }
                    ?>
                    </table>
                  
                <?php
                }else if($tipo_user == 1){
                    $sql = "SELECT * FROM agendamento ORDER BY dia";
                    $dados_agendamento = $conn->query($sql);

                    if($dados_agendamento->num_rows > 0){
                    ?>
                    
                    <table class="styled-table">
                        <thead>
                            <th>Dia</th>
                            <th>Horário</th>
                            <th>Tipo</th>
                            <th>Vet</th>
                            <th>Pet</th>
                            <th>Situação</th>
                            <th>Realizado</th> 
                            <th>Cancelar</th>
                            <th>Excluir</th>
                        </thead>
                        <?php
                        while($exibir = $dados_agendamento->fetch_assoc()){
                    ?>

                    <tbody>
                        <tr>
                            <td><?php echo $exibir['dia'] ;?></td>
                            <td><?php echo $exibir['horario'];?></td>
                            <?php
                                $tipo= $exibir['tipo'];
                                $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                                $dadostipo= $conn->query($sqltipo);
                                $nometipo= $dadostipo->fetch_assoc();
                            ?>
                            <td><?php echo $nometipo['tipo'];?></td>
                            <?php
                                $vet = $exibir['id_veterinario'];
                                $sql_idvet = "SELECT nome, sobrenome, crmv FROM veterinario WHERE id = '$vet'";
                                $dadosidvet= $conn->query($sql_idvet);
                                $vet_agenda= $dadosidvet->fetch_assoc();
                            ?>
                            <td><?php echo $vet_agenda['nome'] . " " . $vet_agenda['sobrenome'] . " | CRMV: " . $vet_agenda['crmv'];?></td>
                            <?php
                                $pet= $exibir['id_pet'];
                                $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                                $dadospet= $conn->query($sqlpet);
                                $nomepet= $dadospet->fetch_assoc();
                            ?>
                            <td><?php echo $nomepet['nome'];?></td>

                            <?php
                                $situacao = $exibir['situacao'];
                                $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                                $dados_situacao= $conn->query($sql_situacao);
                                $situacao_resultado= $dados_situacao->fetch_assoc();
                            ?>
                            <td <?php if($exibir['situacao']==1){?>
                                style = "color: green"
                                <?php }else if($exibir['situacao']==2){?>
                                style = "color: orange"
                                <?php }else{?>
                                style = "color: red" 
                                <?php } ?>
                                ><?php echo $situacao_resultado['situacao'];?></td>
                            
                            <td>
                            <form method="POST" action="editarAgendamento.php?id=<?php echo $exibir['id'] ?>">
                                <input type="checkbox" id="myCheckbox">
                                <label for="myCheckbox">Marcar como realizado</label><br>
                                <button class="bnt-add" onclick="checkInput('myCheckbox')">OK</button>
                            </form>
                            </td>
                            
                            <td><a class="bnt-add" onclick="confirmaCancelamento('<?php echo $exibir['id'];?>')"><i class="bi bi-calendar2-x-fill"></i></a></td>
                            <td><a class="bnt-add" onclick="confirmaExclusao('<?php echo $exibir['id'];?>')"><i class="bi bi-trash3"></i></a></td>
                        </tr>
                    </tbody>

                    <?php
                        }
                        }else{
                            ?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados.</h5>
                            <?php
                        }
                    }
                    ?>
                    </table>
</div> 
   
</section>

        <!--Container Main end-->
        <script>
            var hoje = new Date();
            var dataAtual = hoje.toISOString().split('T')[0];
            document.getElementById('data').min = dataAtual;
            //const days = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];
            //const day = days[new Date().getDay()];
            //document.getElementById('day').innerText = day;

            //const horaAberto = document.getElementById('opening-hour').innerText;
            //const horaFechado = document.getElementById('closing-hour').innerText;

        </script>

        <script>
        function mascara(i,t){

            var v = i.value;

            if(isNaN(v[v.length-1])){
                i.value = v.substring(0, v.length-1);
                return;
            }

            if(t == "cpf"){
                i.setAttribute("maxlength", "14");
            if (v.length == 3 || v.length == 7) i.value += ".";
            if (v.length == 11) i.value += "-";
            }
            if(t == "tel"){
                if(v[0] == 9){
                     i.setAttribute("maxlength", "10");
                    if (v.length == 5) i.value += "-";
                    }else{
                    i.setAttribute("maxlength", "9");
                if (v.length == 4) i.value += "-";
                    }

            }
        }

        function confirmaExclusao(id){
                if(window.confirm("Deseja realmente excluir esse agendamento?")){
                    window.location.href = "excluirAgendamento.php?id=" +  id;
                }
        }

        function confirmaCancelamento(id){
                if(window.confirm("Deseja realmente cancelar esse agendamento?")){
                    window.location.href = "cancelaAgendamento.php?id=" +  id;
                }
        }

        function confirmaNotificacao(id){
                if(window.confirm("Deseja notificar esse cliente?")){
                    window.location.href = "notificaCliente.php?id=" +  id;
                }
        }
        
        </script>
    
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main-agenda.js"></script>

    <?php
        }else{
            echo "Usuário não autenticado.";
        }
    ?>
    </body>

</html>