<?php
    //session_start();
    include_once("conexao.php");

    //if(isset($_SESSION["usuario"])){
        if(isset($_POST["nome"])){
        
            $idV = $_GET["id"];
            $nome = $_POST["nome"];
            $sobrenome = $_POST["sobrenome"];
            $cpf = $_POST["cpf"];
            $datanascimento = $_POST["datanascimento"];
            $email = $_POST["email"];
            $telefone = $_POST["telefone"];
            $cep = $_POST["cep"];
            $complemento = $_POST["complemento"];
            $rua = $_POST["rua"];
            $numero = $_POST["numero"];
            $bairro = $_POST["bairro"];
            $cidade = $_POST["cidade"];
            $estado = $_POST["estado"];
            $pais = $_POST["pais"];
            $formacao = $_POST["formacao"];
            $faculdade = $_POST["faculdade"];
            $crmv = $_POST["crmv"];
              
                $sql_update = "UPDATE veterinario 
                SET nome = '$nome',
                sobrenome = '$sobrenome',
                cpf = '$cpf',
                datanasc = '$datanascimento',
                email = '$email',
                telefone = '$telefone',
                cep = '$cep',
                complemento = '$complemento',
                rua = '$rua',
                num = '$numero',
                bairro = '$bairro',
                cidade = '$cidade',
                estado = '$estado',
                pais = '$pais',
                formacao = '$formacao',
                faculdade = '$faculdade',
                crmv = '$crmv'
                WHERE id = '$idV' ";

                if($conn->query($sql_update) === TRUE){?>
                    <script>
                        alert("Atualizado com sucesso!");
                        history.go(-2);
                    </script>
                    <?php
                }else{?>
                    <script>
                        alert("Erro ao atualizar.");
                        window.history.back();
                    </script>
                    <?php
                }
            }
?>

<!DOCTYPE html>
<html lang="pt">

<head>
    <!-- Favicons -->
  <link href="img/logotipo2.png" rel="icon">
  
    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Edita - TedSol</title>

    <!-- Icons font CSS-->
    <link href="assets/vendor-cadastro/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="assets/vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h1 class="title">Editar</h1>

                    <?php

                    if(isset($_GET["id"])){
                        $id_vet = $_GET["id"];
                        $sql = "SELECT * FROM veterinario WHERE id = '$id_vet'";
                        $consulta = $conn->query($sql);
                        $vet = $consulta->fetch_assoc();
                    }else{
                        echo "usuario não encontrado";
                    }

                    ?>

                    <form method="POST" action="editarVet.php?id=<?php echo $_GET['id'] ?>" enctype="multipart/form-data">
                        <h4>DADOS PESSOAIS</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nome</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="nome" id="nome" value="<?php echo $vet["nome"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Sobrenome</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="sobrenome" id="sobrenome" value="<?php echo $vet["sobrenome"] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">CPF</label>
                                    <input required placeholder = "*" oninput="mascara(this,'cpf')"class="input--style-4" type="text" name="cpf" id="cpf" placeholder="Ex.: xxx.xxx.xxx-xx" autocomplete="off" value="<?php echo $vet["cpf"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Data de nascimento</label>
                                    <div class="input-group-icon">
                                        <input required placeholder = "*" class="input--style-4 js-datepicker" type="date" name="datanascimento" id="datanascimento" value="<?php echo $vet["datanasc"] ?>">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <h4>CONTATO</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input required placeholder = "*" class="input--style-4" type="email" name="email" id="email" value="<?php echo $vet["email"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Telefone</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="telefone" id="telefone" maxlength="15" onkeyup="handlePhone(event)"placeholder="Ex.:(xx)xxxxx-xxxx" autocomplete="off" value="<?php echo $vet["telefone"] ?>"> 

                                </div>
                            </div>
                        </div>

                        <h4>ENDEREÇO</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="cep" class="label">CEP</label>
                                    <input required placeholder = "*" oninput="mascaraCep(this,'cep')" class="input--style-4" type="text" name="cep" id="cep" value="<?php echo $vet["cep"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="complemento" class="label">Complemento</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="complemento" id="complemento" value="<?php echo $vet["complemento"] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Rua</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="rua" id="rua" value="<?php echo $vet["rua"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">N°</label>
                                    <input required placeholder = "*" class="input--style-4" type="number" name="numero" id="numero" value="<?php echo $vet["num"] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Bairro</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="bairro" id="bairro" value="<?php echo $vet["bairro"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Cidade</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="cidade" id="cidade" value="<?php echo $vet["cidade"] ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="form-group col-md-4">
                                <label for="inputState" class="label">Estado</label>
                                <input required placeholder = "*" class="input--style-4" type="text" name="estado" id="estado" value="<?php echo $vet["estado"] ?>">
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">País</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="pais" id="pais" value="<?php echo $vet["pais"] ?>">
                                </div>
                            </div>
                        </div>
                       
                        <h4>IDENTIFICAÇÃO</h4>
                        <br>
                        
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Formação</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="formacao" id="formacao" value="<?php echo $vet["formacao"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Faculdade de Graduação</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="faculdade" id="faculdade" value="<?php echo $vet["faculdade"] ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Crmv</label>
                                    <input required placeholder = "*" class="input--style-4" type="number" name="crmv" id="crmv" value="<?php echo $vet["crmv"] ?>">
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="p-t-15">
                           
                            <button class="btn btn--radius-2 btn--blue" type="submit" value="Salvar" name="bntCadastrar">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        function mascaraCep(cep){
            if (cep.value.length == 5) {
                cep.value = cep.value + '-' 
            }
        }

        function mascara(i,t){

            var v = i.value;

            if(isNaN(v[v.length-1])){
                i.value = v.substring(0, v.length-1);
                return;
            }

            if(t == "cpf"){
                i.setAttribute("maxlength", "14");
            if (v.length == 3 || v.length == 7) i.value += ".";
            if (v.length == 11) i.value += "-";
            }
        }
        
        const handlePhone = (event) => {
        let input = event.target
        input.value = phoneMask(input.value)
        }

        const phoneMask = (value) => {
        if (!value) return ""
        value = value.replace(/\D/g,'')
        value = value.replace(/(\d{2})(\d)/,"($1) $2")
        value = value.replace(/(\d)(\d{4})$/,"$1-$2")
        return value
        }

        (function(){

            const cep = document.querySelector("input[name=cep]");

            cep.addEventListener('blur', e=> {
                const value = cep.value.replace(/[^0-9]+/, '');
            const url = `https://viacep.com.br/ws/${value}/json/`;

            fetch(url)
            .then( response => response.json())
            .then( json => {
                    
                if( json.logradouro ) {
                    document.querySelector('input[name=rua]').value = json.logradouro;
                    document.querySelector('input[name=bairro]').value = json.bairro;
                    document.querySelector('input[name=cidade]').value = json.localidade;
                    document.querySelector('input[name=estado]').value = json.uf;
                }

            });

            });

        })();
        
    </script>

</body>

</html>