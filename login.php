<?php

    require_once('conexao.php');
    session_start();

    if(isset($_POST['email']) || isset($_POST['senha']) || isset($_POST['opcao'])){

        if(strlen($_POST['email']) == 0){
            ?>
            <script>alert('Preencha o campo de email.')</script>
            <?php
        }else if(strlen($_POST['senha']) == 0){
            ?>
            <script>alert('Preencha o campo de senha.')</script>
            <?php
        }else if(!isset($_POST['opcao'])){
            ?>
            <script>alert('Preencha o tipo de usuário.')</script>
            <?php
        }else{

            $email = $_POST['email'];
            $senha = $_POST['senha'];
            $tipo = $_POST['opcao'];

            if($tipo == '2' || $tipo == '1'){
                //VETERINÁRIO
                $sql_code_vet = "SELECT * FROM veterinario WHERE email = '$email' AND senha = '$senha' AND tipo_usuario = '$tipo'"; 
                $sql_query_vet = $conn->query($sql_code_vet) or die("Falha na execução: " . $conn->error);
                $quantidade_vet = $sql_query_vet->num_rows;

                if($quantidade_vet == 1){
                    $usuario = $sql_query_vet->fetch_assoc();
                    $_SESSION['usuario'] = $usuario;
                    $_SESSION['id'] = $usuario['id'];
                    $_SESSION['nome'] = $usuario['nome'];
                    $_SESSION['sobrenome'] = $usuario['sobrenome'];
                    $_SESSION['cpf'] = $usuario['cpf'];
                    $_SESSION['datanasc'] = $usuario['datanasc'];
                    $_SESSION['email'] = $usuario['email'];
                    $_SESSION['telefone'] = $usuario['telefone'];
                    $_SESSION['rua'] = $usuario['rua'];
                    $_SESSION['num'] = $usuario['num'];
                    $_SESSION['bairro'] = $usuario['bairro'];
                    $_SESSION['cidade'] = $usuario['cidade'];
                    $_SESSION['estado'] = $usuario['estado'];
                    $_SESSION['pais'] = $usuario['pais'];
                    $_SESSION['formacao'] = $usuario['formacao'];
                    $_SESSION['faculdade'] = $usuario['faculdade'];
                    $_SESSION['crmv'] = $usuario['crmv'];
                    $_SESSION['senha'] = $usuario['senha'];
                    $_SESSION['foto_perfil'] = $usuario['foto_perfil'];
                    $_SESSION['tipo_usuario'] = $usuario['tipo_usuario'];
                        
                    header("Location: inicioVet.php");
                }else{
                    ?>
                    <script>alert('Usuário não encontrado.')</script>
                    <?php 
                }
                
            }else if($tipo == '3'){
                $sql_code = "SELECT * FROM clientes WHERE email = '$email' AND senha = '$senha' AND tipo_usuario = '$tipo'";
                
                $sql_query = $conn->query($sql_code) or die("Falha na execução: " . $conn->error);

                $quantidade = $sql_query->num_rows;

                if($quantidade == 1){

                    $usuario = $sql_query->fetch_assoc();
                   
                    $_SESSION['usuario'] = $usuario;
                    $_SESSION['id'] = $usuario['id'];
                    $_SESSION['nome'] = $usuario['nome'];
                    $_SESSION['sobrenome'] = $usuario['sobrenome'];
                    $_SESSION['cpf'] = $usuario['cpf'];
                    $_SESSION['datanasc'] = $usuario['datanasc'];
                    $_SESSION['email'] = $usuario['email'];
                    $_SESSION['telefone'] = $usuario['telefone'];
                    $_SESSION['rua'] = $usuario['rua'];
                    $_SESSION['num'] = $usuario['num'];
                    $_SESSION['bairro'] = $usuario['bairro'];
                    $_SESSION['cidade'] = $usuario['cidade'];
                    $_SESSION['estado'] = $usuario['estado'];
                    $_SESSION['pais'] = $usuario['pais'];
                    $_SESSION['senha'] = $usuario['senha'];
                    $_SESSION['tipo_usuario'] = $usuario['tipo_usuario'];

                    header("Location: inicioCliente.php");
                }else{
                    ?>
                    <script>alert('Usuário não encontrado.')</script>
                    <?php 
                }

        }else{
            ?>
            <script>alert('Usuário não existe.')</script>
            <?php 
        }
    }
}
?>

<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
    <link href="img/logotipo2.png" rel="icon">

    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Login</title>

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">

                    <h2 class="title">Login</h2>

                    <form method="POST">
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input class="input--style-4" type="email" name="email" id="email">
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Senha</label>
                                    <input required placeholder = " " class="input--style-4" type="password" name="senha" id="password">
                                    <!--<span  class="password-toggle" id="toggle-password"  onclick="mostrarOcultarSenha()">Mostrar</span>-->
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="radio-container">
                                <label class="radio-label">
                                    <input type="radio" name="opcao" class="radio-input" value="1">
                                Clínica
                                </label>
                                <label class="radio-label">
                                    <input type="radio" name="opcao" class="radio-input" value="2">
                                Veterinário
                                </label>
                                <label class="radio-label">
                                    <input type="radio" name="opcao" class="radio-input" value="3">
                                Cliente
                                </label>
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn--radius-2 btn--blue">Entrar</button>
                        <br>
                        <br>
                        <a href="esqueciSenha.php">Esqueci minha senha</a>
                      
                    </form>
                </div>
                <div class="image-container">
                <img src="img/sobre.png" alt="">
                </div>
            </div>
        </div>
        
    </div>

    <script>
        function mostrarOcultarSenha() {
            const passwordInput = document.getElementById("password");
            const toggleButton = document.getElementById("toggle-password");

            toggleButton.addEventListener("click", function() {
            if (passwordInput.type === "password") {
                passwordInput.type = "text";
                toggleButton.textContent = "Ocultar";
            } else {
                passwordInput.type = "password";
                toggleButton.textContent = "Mostrar";
            }
            });
        }
    </script>
</body>

</html>