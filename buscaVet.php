<?php
include_once("conexao.php");
session_start();

$pesquisa = $_POST["pesquisa"];

    //iniciando sessão
    if(isset($_SESSION["id"])){
        $tipo_user = $_SESSION['tipo_usuario'];

                        $sql_2 = "SELECT *
                        FROM veterinario 
                        WHERE nome like '%$pesquisa%'
                        or sobrenome like '%$pesquisa%'
                        ORDER BY nome";

                        $dados_vet = $conn->query($sql_2);

                        if($dados_vet->num_rows > 0){
?>
 <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>CRMV</th>
                        <th>Formação</th>
                        <th>Contatos</th>

                        <?php if($tipo_user == 1){?>
                        <th>Editar</th>
                        <th>Excluir</th>
                        <?php } ?>

                    </tr>
                </thead>

                <?php
                    while($exibir = $dados_vet->fetch_assoc()){
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'] ." ". $exibir['sobrenome'];?></td>
                        <td><?php echo $exibir['cpf'];?></td>
                        <td><?php echo $exibir['crmv'] ;?></td>
                        <td><?php echo $exibir['formacao'] ;?></td>
                        <td><?php echo $exibir['email'] ;?> <br> <?php echo $exibir['telefone'] ;?></td>
                        <?php if($tipo_user == 1){?>

                        <td><a href="editarVet.php?id=<?php echo $exibir['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a style="font-color: blue"
                                onclick="confirmaExclusao('<?php echo $exibir['id'];?>')"><i class="bi bi-trash3"></i></a></td>

                                <?php } ?>
                    </tr>
                </tbody>

                <?php
                    }
                }else{
                    echo "Não há veterinários cadastrados.";
                }
            }
                ?>

            </table>