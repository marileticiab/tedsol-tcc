<?php

include_once("conexao.php");

if(isset($_POST["nome"])){

    $nome = $_POST["nome"];
    $peso = $_POST["peso"];
    $raca = $_POST["raca"];
    $datanascimento = $_POST["datanasc_pet"];
    $genero = $_POST["genero"];
    $cor = $_POST["cor"];
    $especie = $_POST["especie"];
    $cliente = $_POST["cliente"];

        $file = $_FILES["file"];
        
        $folder = "uploads";

        $permite = array("tif", "jpg", "png", "pdf", "jfif");
        $maxSize = 1024 * 1024 * 5;

        $msg = array();
        $erroMsg = array( 1 => "O arquivo é maior do que o permitido." ,
                        2 => "Não foi possível fazer o upload.");
            
        $name = $file["name"];
        $type = $file["type"];    
        $size = $file["size"];  
        $error = $file["error"];  
        $tmp = $file["tmp_name"]; 
            
        $extensao = @end(explode(".", $name));  
        
        $novoNome = rand() . ".$extensao";

        if($error != 0){
            $msg[] = "<b>$name</b>" . $erroMsg[$error];

        }else if(!in_array($extensao, $permite)){
            $msg[] = "<b>$name</b>" . "Tipo de arquivo não suportado.";
            
        }else if($size > $maxSize){
            $msg[] = "<b>$name</b>" . "Tamanho maior que o permitido.";
            
        }else{
            if(move_uploaded_file($tmp, $folder."/".$novoNome)){

                $sql = "INSERT INTO pet (nome, peso, raca, datanasc, genero, cor, especie, foto_perfil, id_cliente) 
                VALUES('$nome', $peso, '$raca', '$datanascimento', '$genero', '$cor', '$especie', '$novoNome', '$cliente')";

                    if($conn->query($sql) === TRUE){
                        ?>
                        <script>
                            alert("Cadastro realizado com sucesso!");
                            history.go(-2);
                        </script>

                        <?php
                    }else{
                        ?>
                        <script>
                            alert("Falha ao cadastrar!");
                            window.history.back();
                        </script>
             <?php
                }
            }
        }
}

?>