<?php
    include_once("conexao.php");

    if(isset($_POST["nome"])){
       
        $idC = $_GET["id"];
        $nome = $_POST["nome"];
        $sobrenome = $_POST["sobrenome"];
        $cpf = $_POST["cpf"];
        $datanascimento = $_POST["datanascimento"];
        $email = $_POST["email"];
        $telefone = $_POST["telefone"];
        $cep = $_POST["cep"];
        $complemento = $_POST["complemento"];
        $rua = $_POST["rua"];
        $numero = $_POST["numero"];
        $bairro = $_POST["bairro"];
        $cidade = $_POST["cidade"];
        $estado = $_POST["estado"];
        $pais = $_POST["pais"];

        $sql_update = "UPDATE clientes 
        SET nome = '$nome',
        sobrenome = '$sobrenome',
        cpf = '$cpf',
        datanasc = '$datanascimento',
        email = '$email',
        telefone = '$telefone',
        cep = '$cep',
        complemento = '$complemento',
        rua = '$rua',
        num = '$numero',
        bairro = '$bairro',
        cidade = '$cidade',
        estado = '$estado',
        pais = '$pais'
        WHERE id = '$idC' ";

        if($conn->query($sql_update) === TRUE){?>
            <script>
                alert("Atualizado com sucesso!");
                history.go(-2);          
            </script>
            <?php
        }else{?>
            <script>
                alert("Erro ao atualizar.");
                window.history.back();
            </script>
            <?php
        }
    }
?>

<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
  <link href="img/logotipo2.png" rel="icon">
  
    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Editar - TedSol</title>

    <!-- Icons font CSS-->
    <link href="assets/vendor-cadastro/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="assets/vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h1 class="title">Editar Cliente</h1>

                <?php

                    if(isset($_GET["id"])){
                        $id_cliente = $_GET["id"];
                        $sql = "SELECT * FROM clientes WHERE id = '$id_cliente'";
                        $consulta = $conn->query($sql);
                        $cliente = $consulta->fetch_assoc();
                    }else{
                        echo "usuario não encontrado";
                    }

                    ?>

                    <form method="POST" action="editarCliente.php?id=<?php echo $_GET['id'] ?>">
                        <h4>DADOS PESSOAIS</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nome</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="nome" value="<?php echo $cliente["nome"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Sobrenome</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="sobrenome" value="<?php echo $cliente["sobrenome"] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">CPF</label>
                                        <input required placeholder = "*" value="<?php echo $cliente["cpf"] ?>" oninput="mascara(this,'cpf')"class="input--style-4" type="text" name="cpf" id="cpf"placeholder="Ex.: xxx.xxx.xxx-xx" autocomplete="off" >
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Data de nascimento</label>
                                    <div class="input-group-icon">
                                        <input required placeholder = "*" class="input--style-4 js-datepicker" type="date" name="datanascimento" value="<?php echo $cliente["datanasc"] ?>">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <h4>CONTATO</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input required placeholder = "*" class="input--style-4" type="email" name="email" value="<?php echo $cliente["email"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Telefone</label>
                                        <input required placeholder = "*" oninput="mascara(this,'tel')" class="input--style-4" type="text" name="telefone" id="telefone" placeholder="Ex.: xxxxx-xxxx" autocomplete="off" value="<?php echo $cliente["telefone"] ?>">
                                </div>
                            </div>
                        </div>

                        <h4>ENDEREÇO</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="cep" class="label">CEP</label>
                                    <input required placeholder = "*" oninput="mascaraCep(this,'cep')" class="input--style-4" type="text" name="cep" id="cep" value="<?php echo $cliente["cep"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="complemento" class="label">Complemento</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="complemento" id="complemento" value="<?php echo $cliente["complemento"] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Rua</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="rua" value="<?php echo $cliente["rua"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">N°</label>
                                    <input required placeholder = "*" class="input--style-4" type="number" name="numero" value="<?php echo $cliente["num"] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Bairro</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="bairro" value="<?php echo $cliente["bairro"] ?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Cidade</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="cidade" value="<?php echo $cliente["cidade"] ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="form-group col-md-4">
                                <label for="inputState" class="label">Estado</label>
                                <input required placeholder = "*" class="input--style-4" type="text" name="estado" value="<?php echo $cliente["estado"] ?>">
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">País</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="pais" value="<?php echo $cliente["pais"] ?>">
                                </div>
                            </div>
                        </div>

                        <div class="p-t-15">
                           
                            <!--<button class="btn btn--radius-2 btn--blue" onclick= "redirecionar()"; type="submit">Cadastrar</button>-->
                            <button class="btn btn--radius-2 btn--blue" type="submit" value="Salvar" name="bntCadastrar">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function mascaraCep(cep){
            if (cep.value.length == 5) {
                cep.value = cep.value + '-' 
            }
        }

        function mascara(i,t){

            var v = i.value;

            if(isNaN(v[v.length-1])){
                i.value = v.substring(0, v.length-1);
                return;
            }

            if(t == "cpf"){
                i.setAttribute("maxlength", "14");
            if (v.length == 3 || v.length == 7) i.value += ".";
            if (v.length == 11) i.value += "-";
            }
            if(t == "tel"){
                if(v[0] == 9){
                     i.setAttribute("maxlength", "10");
                    if (v.length == 5) i.value += "-";
                    }else{
                    i.setAttribute("maxlength", "9");
                if (v.length == 4) i.value += "-";
                    }

            }
        }

        (function(){

            const cep = document.querySelector("input[name=cep]");

            cep.addEventListener('blur', e=> {
                const value = cep.value.replace(/[^0-9]+/, '');
            const url = `https://viacep.com.br/ws/${value}/json/`;

            fetch(url)
            .then( response => response.json())
            .then( json => {
                    
                if( json.logradouro ) {
                    document.querySelector('input[name=rua]').value = json.logradouro;
                    document.querySelector('input[name=bairro]').value = json.bairro;
                    document.querySelector('input[name=cidade]').value = json.localidade;
                    document.querySelector('input[name=estado]').value = json.uf;
                }

            });

            });

            })();
    </script>

</body>

</html>