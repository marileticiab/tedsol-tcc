<?php
include_once('conexao.php');
session_start();
?>

<!doctype html>
<html lang="pt">

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
    @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

</head>

<body className='snippet-body'>

    <?php
    if(isset($_SESSION["usuario"])){
        require_once("menuVet.php");
    ?>
    <!--Container Main start-->

    <div class="container">
        <h1 class="title-model">Veterinários Contato</h1>
        <!--Barra de pesquisa start-->
            <?php
                $sql = "SELECT nome, sobrenome, telefone, email FROM veterinario ORDER BY nome";
                $dados_vet = $conn->query($sql);

                if($dados_vet->num_rows > 0){
                    ?>
            <br>
            <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                    </tr>
                </thead>

                <?php
                    while($exibir = $dados_vet->fetch_assoc()){
                        $telefone = $exibir['telefone'];
                        $telefone = preg_replace('/[^0-9]/', '', $telefone);
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'] ." ". $exibir['sobrenome'];?></td>
                        <td><a href="https://wa.me/55<?php echo $telefone?>" target="_blank"> <i class="bi bi-whatsapp" style="font-size: 2em; color: green"></i> </a></td>
                        <td><i class="bi bi-envelope-at-fill" style="font-size: 2em; color: #ed8975"></i><?php echo "  " . $exibir['email']?></td>
                    </tr>
                </tbody>

                <?php
                    }
                }else{
                    echo "Não há veterinários cadastrados.";
                }
            
                ?>
            </table>
        </div>

        <div class="container">
        <h1 class="title-model">Clientes Contato</h1>
        <br>
        <br>
        <div class="resultados">
            
            <?php
                $sql_C = "SELECT nome, sobrenome, telefone, email FROM clientes ORDER BY nome";
                $dados_cliente = $conn->query($sql_C);

                if($dados_cliente->num_rows > 0){
                    ?>

            <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                    </tr>
                </thead>

                <?php
                    while($exibir = $dados_cliente->fetch_assoc()){
                        $telefone = $exibir['telefone'];
                        $telefone = preg_replace('/[^0-9]/', '', $telefone);
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'] ." ". $exibir['sobrenome'];?></td>
                        <td><a href="https://wa.me/55<?php echo $telefone?>" target="_blank"> <i class="bi bi-whatsapp" style="font-size: 2em; color: green"></i> </a></td>
                        <td><i class="bi bi-envelope-at-fill" style="font-size: 2em; color: #ed8975"></i><?php echo "  " . $exibir['email']?></td>
                    </tr>
                </tbody>

                <?php
                    }
                }else{
                    echo "Não há clientes cadastrados.";
                }
            }
                ?>
            </table>
        <!--Barra de pesquisa end-->
        <br>
        <br>

    <!--Container Main end-->
    <?php
        /*}else{
            echo "Usuário não autenticado.";
        }*/
    ?>

</body>

</html>