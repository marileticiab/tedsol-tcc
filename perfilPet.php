<?php
include_once('conexao.php');
session_start();
?>
<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
    @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <script>
function goBack() {
    window.history.back()
}
</script>
</head>

<body className='snippet-body'>

    <body id="body-pd">
    <?php
    if(isset($_SESSION["usuario"])){
        $usuario = $_SESSION["tipo_usuario"];

        if($usuario == 1 || $usuario == 2){
            require_once("menuVet.php");
        }else if($usuario == 3){
            require_once("menuCliente.php");
        }
    ?>
    <!--Container Main start-->
       
            <div class="container">

            <h1 class="title-model">Perfil do Pet</h1>
            <br>
            <br>
            
            <?php
            $idPet = $_GET["id"];
            $sqlPet = "SELECT * FROM pet WHERE id = $idPet";

            $consultaPet = $conn->query($sqlPet);
            $dadosPet = $consultaPet->fetch_assoc();
            ?>

            <!--Foto de perfil-->
            <h6>Foto de Perfil</h6>
            <div class="foto-perfil" id="foto-perfil">
                <img src="uploads/<?php echo $dadosPet['foto_perfil'] ?>" height="auto" width="200">
            </div>
            <br>
                <a href="editarImgPet.php?id=<?php echo $dadosPet['id']?>" value="update"><i class="bi bi-pencil-square"></i>Adicionar/Editar Foto</a>
            <br>
            <br>
            <h6><a href="cartaoVacinaPet.php?id=<?php echo $dadosPet['id'];?>">Cartão de Vacina do Pet <i class="bi bi-postcard"></i></a></h6>

            <table class="styled-table">
                <thead>
                    <tr>
                        <th colspan="3">Nome do Pet</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="3"> <?php echo $dadosPet['nome']; ?></td>

                    </tr>
                </tbody>
                
                <thead>
                    <tr>
                        <th>Espécie</th>
                        <th>Raça</th>
                        <th>Data de Nasc.</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>

                        <td>
                            <?php echo $dadosPet['especie']; ?>
                        </td>
                        <td>
                            <?php echo $dadosPet['raca']; ?>
                        </td>
                        <td>
                            <?php echo $dadosPet['datanasc']; ?>
                        </td>

                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>Gênero</th>
                        <th>Peso</th>
                        <th>Cor do Pelo</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php echo $dadosPet['genero']; ?>
                        </td>
                        <td>
                            <?php echo $dadosPet['peso']; ?>
                        </td>
                        <td>
                            <?php echo $dadosPet['cor']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
       <?php
       }?>
           
    </body>

</html>