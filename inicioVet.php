<?php
session_start();
include_once('conexao.php');
?>

<!DOCTYPE html>
<html>

<head>  
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
        @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

</head>

<body className='snippet-body'>

    <?php
        if(isset($_SESSION["usuario"])){
            require_once("menuVet.php");
    ?>

        <!--Container Main start-->

        <div class="container">
            
            <header class="head-info">
            <h1 >Bem-vinda(o) veterinária(o) administradora(o),</h1>
            <h1 > <?php echo $_SESSION['nome'];?>! </h1>

            <div class="image-container">
                <img src="img/sobre.png" alt="">
            </div>
                <br>
                <br> 
                <br>
                <nav>
                    <ul>
                        <li><a href="agenda.php">Agenda</a></li>
                        <li><a href="showClientes.php">Clientes</a></li>
                        <li><a href="showVets.php">Veterinários</a></li>
                        <li><a href="showPets.php">Pets</a></li>
                        <li><a href="mensagemVet.php">Mensagens</a></li>
                    </ul>
                </nav>
                <br>
                <br><br>
                <div class="image-container-2">
                <img src="img/patinhas.png" alt="">
                </div>
                <!--<a href="https://www.instagram.com/tedsoltcc/" target="_blank">Siga-nos!</a>-->
        
            </header>
            <br>
            <br><br>
            <main class="body-inicio">
            <h2> Esse é o seu sistema veterinária(o)! <br> O que gostaria de acessar hoje?</h2>
                <br><br><br>

                <h3>Seus Agendamentos</h3>
                <!--consultas marcadas-->
                <?php

                    // Get current date and time in Brasilia timezone
                    date_default_timezone_set('America/Sao_Paulo');
                    $currentDateAndTime = new DateTime();

                    // Get date components
                    $day = $currentDateAndTime->format('d');
                    $month = $currentDateAndTime->format('m');
                    $year = $currentDateAndTime->format('Y');

                    // Get time components
                    $hours = $currentDateAndTime->format('H');
                    $minutes = $currentDateAndTime->format('i');
                    $seconds = $currentDateAndTime->format('s');

                    // Output the current date and time in Brasilia timezone
                    //echo "Data e Hora atual em Brasília: " . $currentDateAndTime->format('Y-m-d H:i:s') . "\n";

                $dataatual=$currentDateAndTime->format('Y-m-d');
                $vet =  $_SESSION['id'];
                $tipo_vet = $_SESSION['tipo_usuario'];

                if($tipo_vet == 2){
                    $sql = "SELECT * FROM agendamento WHERE id_veterinario  = '$vet' AND dia >= '" .$dataatual. "' ORDER BY dia";
                    $dados_agendamento = $conn->query($sql);

                    if($dados_agendamento->num_rows > 0){
                    ?>
                    
                    <table class="styled-table">
                        <thead>
                            <th>Dia</th>
                            <th>Horário</th>
                            <th>Tipo</th>
                            <th>Pet</th>
                            <th>Situação</th>
                        </thead>
                        <?php
                        while($exibir = $dados_agendamento->fetch_assoc()){
                    ?>

                    <tbody>
                        <tr>
                            <td><?php echo $exibir['dia'] ;?></td>
                            <td><?php echo $exibir['horario'];?></td>
                            <?php
                                $tipo= $exibir['tipo'];
                                $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                                $dadostipo= $conn->query($sqltipo);
                                $nometipo= $dadostipo->fetch_assoc();
                            ?>
                            <td><?php echo $nometipo['tipo'];?></td>
                            <?php
                                $pet= $exibir['id_pet'];
                                $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                                $dadospet= $conn->query($sqlpet);
                                $nomepet= $dadospet->fetch_assoc();
                            ?>
                            <td><?php echo $nomepet['nome'];?></td>
                            <?php
                                $situacao = $exibir['situacao'];
                                $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                                $dados_situacao= $conn->query($sql_situacao);
                                $situacao_resultado= $dados_situacao->fetch_assoc();
                            ?>
                           <td <?php if($exibir['situacao']==1){?>
                                style = "color: green"
                                <?php }else if($exibir['situacao']==2){?>
                                style = "color: orange"
                                <?php }else{?>
                                style = "color: red" 
                                <?php } ?>
                                ><?php echo $situacao_resultado['situacao'];?></td>
                            
                        </tr>
                    </tbody>

                    <?php
                        }
                        }else{
                            ?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados.</h5>
                            <?php
                        }
                    ?>
                    </table>
                <?php
                }else if($tipo_vet == 1){
                    $sql = "SELECT * FROM agendamento WHERE dia >= '" .$dataatual. "' ORDER BY dia";
                    $dados_agendamento = $conn->query($sql);

                    if($dados_agendamento->num_rows > 0){
                    ?>
                    
                    <table class="styled-table">
                        <thead>
                            <th>Dia</th>
                            <th>Horário</th>
                            <th>Tipo</th>
                            <th>Vet</th>
                            <th>Pet</th>
                            <th>Situação</th>
                        </thead>
                        <?php
                        while($exibir = $dados_agendamento->fetch_assoc()){
                    ?>

                    <tbody>
                        <tr>
                            <td><?php echo $exibir['dia'] ;?></td>
                            <td><?php echo $exibir['horario'];?></td>
                            <?php
                                $tipo= $exibir['tipo'];
                                $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                                $dadostipo= $conn->query($sqltipo);
                                $nometipo= $dadostipo->fetch_assoc();
                            ?>
                            <td><?php echo $nometipo['tipo'];?></td>
                            <?php
                                $vet = $exibir['id_veterinario'];
                                $sql_idvet = "SELECT nome, sobrenome, crmv FROM veterinario WHERE id = '$vet'";
                                $dadosidvet= $conn->query($sql_idvet);
                                $vet_agenda= $dadosidvet->fetch_assoc();
                            ?>
                            <td><?php echo $vet_agenda['nome'] . " " . $vet_agenda['sobrenome'] . " | CRMV: " . $vet_agenda['crmv'];?></td>
                            <?php
                                $pet= $exibir['id_pet'];
                                $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                                $dadospet= $conn->query($sqlpet);
                                $nomepet= $dadospet->fetch_assoc();
                            ?>
                            <td><?php echo $nomepet['nome'];?></td>

                            <?php
                                $situacao = $exibir['situacao'];
                                $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                                $dados_situacao= $conn->query($sql_situacao);
                                $situacao_resultado= $dados_situacao->fetch_assoc();
                            ?>
                             <td <?php if($exibir['situacao']==1){?>
                                style = "color: green"
                                <?php }else if($exibir['situacao']==2){?>
                                style = "color: orange"
                                <?php }else{?>
                                style = "color: red" 
                                <?php } ?>
                                ><?php echo $situacao_resultado['situacao'];?></td>
                            
                            </tr>
                    </tbody>

                    <?php
                        }
                        }else{
                            ?>
                            <h5 style = "padding-left: 50px">Não há agendamentos cadastrados.</h5>
                            <?php
                        }
                    ?>
                    </table>
                <?php
                }
                ?>

                <br><br><br><br>
                <?php
                                        
                    $sql = "SELECT * FROM horario_funcionamento;";
                    $sql_query = $conn->query($sql);

                    $quantidade = $sql_query->num_rows;

                    if($quantidade > 0){
                    $horario = $sql_query->fetch_assoc();
                    }
                ?>
                <h3>Horários de Funcionamento</h3>
                <form class="data-container" action="inserirHorario.php" method="POST">
                    <div class="data-content">
                    <div class="opening-hours">
                        <span>Aberto:</span>
                        <input name="aberto" id="opening-hour" type="time" value="<?php echo $horario["aberto"] ?>"></input>
                    </div>
                    <div class="closing-hours">
                        <span>Fechado:</span>
                        <input name="fechado" id="closing-hour" type="time"  value="<?php echo $horario["fechado"] ?>"></input>
                    </div>

                    <button align="center" class="bnt-add" id="ok-button" type="submit" value="OK" name="bntValidar">OK</button>
                    </div>
                </form>
            </main>
            <br><br><br><br>
            <footer>
                <a href="https://www.instagram.com/tedsoltcc/" target="_blank">@tedsoltcc</a>
            </footer>
        </div>
    
    <?php
        }else{
            echo "Usuário não autenticado.";
        }
    ?>
    </body>

</html>