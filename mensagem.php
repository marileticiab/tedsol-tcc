<?php
include_once('conexao.php');
session_start();
?>

<!doctype html>
<html lang="pt">

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
    @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <script>
    $(document).ready(function() {
        $("#form-pesquisa").submit(function(evento) {

            evento.preventDefault();
            let pesquisa = $("#pesquisa").val();
            let dados = {
                pesquisa: pesquisa
            }

            //alert(pesquisa);

            $.post("buscaVetMensagem.php", dados, function(retorna) {
                $(".resultados").html(retorna);
            });
        });
    });
    </script>
</head>

<body className='snippet-body'>

    <?php
    if(isset($_SESSION["usuario"])){
        require_once("menuCliente.php");
    ?>
    <!--Container Main start-->

    <div class="container">
        <h1 class="title-model">Veterinários Contato</h1>
        <h5>Entre em contato aqui com seu veterinário!</h5>
        <br>
        <!--Barra de pesquisa start-->
        <form name="form-pesquisa" id="form-pesquisa" method="POST">
            <div class="row row-space">
                <div class="col-2">
                    <div class="input-group">
                        <input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquise nomes...">
                    </div>
                    <br>
                    <div class="input-group">
                        <input class="bnt-add" type="submit" name="enviar" value="Pesquisar">
                    </div>
                </div>
            </div>
        </form>

        <div class="resultados">
            
            <?php
                $sql = "SELECT nome, sobrenome, telefone, email FROM veterinario ORDER BY nome";
                $dados_vet = $conn->query($sql);

                if($dados_vet->num_rows > 0){
                    ?>

            <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                    </tr>
                </thead>

                <?php
                    while($exibir = $dados_vet->fetch_assoc()){
                        $telefone = $exibir['telefone'];
                        $telefone = preg_replace('/[^0-9]/', '', $telefone);
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'] ." ". $exibir['sobrenome'];?></td>
                        <td><a href="https://wa.me/55<?php echo $telefone?>" target="_blank"> <i class="bi bi-whatsapp"></i> </a></td>
                        <td><?php echo $exibir['email']?></td>
                    </tr>
                </tbody>

                <?php
                    }
                }else{
                    echo "Não há veterinários cadastrados.";
                }
            }
                ?>
            </table>
        </div>

        <!--Barra de pesquisa end-->
        <br>
        <br>
    </div>

    <!--Container Main end-->
    <?php
        /*}else{
            echo "Usuário não autenticado.";
        }*/
    ?>

</body>

</html>