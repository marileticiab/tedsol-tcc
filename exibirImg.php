<?php

include_once("conexao.php");
//session_start();
$tipo_user = $_SESSION['tipo_usuario'];
$id_user = $_SESSION['id'];

$sql = "SELECT * FROM imagens ORDER BY id desc";
$resultado = $conn->query($sql);
//echo $sql;

if($resultado->num_rows > 0){
    while($exibir = $resultado->fetch_assoc()){

        if($exibir['id_tipo-user'] == $tipo_user){
            if($exibir['id_user'] == $id_user){
        ?>

        <div class="imagens">
            <img src="uploads/<?php echo $exibir['nome'] ?>" height="auto" width="100">
            <br>
            <a href="#" onclick="confirmarExclusao('<?php echo $exibir['id']?>')" value="delete"><i class="bi bi-trash3"></i></a>
            <!--<a href="editarImg.php?id=<?php echo $exibir['id']?>" value="update"><i class="bi bi-pencil-square"></i></a>-->
        </div>

        <?php
            }
        }else{
            echo "Você não tem uma foto de perfil ainda. Adicione!";
        }
    }
}
?>

<script>
    function confirmarExclusao(id){
        if(window.confirm("Deseja realmente excluir a imagem?")){
            window.location = "excluirImg.php?id=" + id;
        }
    }
</script>