<?php
    include_once("conexao.php");

    if(isset($_FILES["file"])){
       
        $id = $_GET["id"];
        $tipo_usuario = $_GET["tipo_usuario"];

        $file = $_FILES["file"];

        $folder = "uploads";

        $permite = array("tif", "jpg", "png", "pdf", "jfif", "webp");
        $maxSize = 1024 * 1024 * 5;

        $msg = array();
        $erroMsg = array( 1 => "O arquivo é maior do que o permitido." ,
                            2 => "Não foi possível fazer o upload.");
        
        $name = $file["name"];
        $type = $file["type"];    
        $size = $file["size"];  
        $error = $file["error"];  
        $tmp = $file["tmp_name"]; 
        
        $extensao = @end(explode(".", $name));  
    
        $novoNome = rand() . ".$extensao";

        if($error != 0){
            $msg[] = "<b>$name</b>" . $erroMsg[$error];
            ?>
            <script>
                alert("Erro ao realizar upload!");
                 window.history.back();
            </script>
            <?php

        }else if(!in_array($extensao, $permite)){
            $msg[] = "<b>$name</b>" . "Tipo de arquivo não suportado.";
            ?>
            <script>
                alert("Tipo de arquivo não suportado.");
                 window.history.back();
            </script>
            <?php
        }else if($size > $maxSize){
            $msg[] = "<b>$name</b>" . "Tamanho maior que o permitido.";
            ?>
            <script>
                alert("Tamanho maior que o permitido.");
                 window.history.back();
            </script>
            <?php
        }else{
            if(move_uploaded_file($tmp, $folder."/".$novoNome)){

                if($tipo_usuario == 3){
                    //cliente
                    $sql_update = "UPDATE clientes SET foto_perfil = '$novoNome' WHERE id = '$id'";
                }else{
                    //vet ou adm
                    $sql_update = "UPDATE veterinario SET foto_perfil = '$novoNome' WHERE id = '$id'";
                }

                if($conn->query($sql_update) === TRUE){?>
                    <script>
                        alert("Atualizado com sucesso!");
                        history.go(-2);          
                    </script>
                    <?php
                }else{?>
                    <script>
                        alert("Erro ao atualizar.");
                        window.history.back();
                    </script>
                    <?php
                }
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
    <link href="img/logotipo2.png" rel="icon">

    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Editar - TedSol</title>

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">

                    <h2 class="title">Editar Foto</h2>

                    <form method="POST" enctype="multipart/form-data" action="editarImg.php?id=<?php echo $_GET['id'] ?>&tipo_usuario=<?php echo $_GET['tipo_usuario']?>">
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nova foto de perfil</label>
                                    <label for="file">Selecione o arquivo</label>
                                    <input type="file" name="file" id="file" required>
                                    <!--<input type="submit" name="upload" value="Enviar">-->
                                    <br>
                                </div>
                            </div>
                        </div>

                        <button type="submit" name="upload" value="Enviar" class="btn btn--radius-2 btn--blue">OK</button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>