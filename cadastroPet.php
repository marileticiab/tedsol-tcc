<?php
session_start();
?>

<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
  <link href="img/logotipo2.png" rel="icon">
  
    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Cadastro - TedSol</title>

    <!-- Icons font CSS-->
    <link href="assets/vendor-cadastro/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="assets/vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
<?php
    //iniciando sessão
    if(isset($_SESSION["usuario"])){
        $tipo_user = $_SESSION['tipo_usuario'];
?>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h1 class="title">Cadastro Pet</h1>
                    <form method="POST" action="inserirPet.php" enctype="multipart/form-data">
                        <h4>DADOS</h4>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nome</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="nome">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Peso (Kg)</label>
                                    <input required placeholder = "*" class="input--style-4" type="number" name="peso">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Raça</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="raca">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Data de nascimento</label>
                                    <div class="input-group-icon">
                                        <input required placeholder = "*" class="input--style-4 js-datepicker" type="date" name="datanasc_pet">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Gênero</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="genero">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Cor</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="cor">
                                </div>
                            </div>
                            
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Espécie</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="especie">
                                </div>
                            </div>
                        </div>

                        <h4>DADOS TUTOR</h4>
                        <br>
                        <div class="row row-space">
                            
                            <div class="col-2">
                                <div class="input-group">
                                   
                                   <?php
                                    ?>
                                    <select name="cliente">
                                    
                                            <?php
                                                if($tipo_user == 1 || $tipo_user == 2){
                                                    include_once('conexao.php');
                                                    $sql = "SELECT id, nome, sobrenome, cpf FROM clientes ORDER BY nome";

                                                    $cliente = $conn->query($sql);

                                                    while($row_cliente = $cliente->fetch_assoc()){
                                                        ?>
                                                            <option value= "<?php echo $row_cliente["id"]; ?>"> <?php echo $row_cliente["nome"] ." ". $row_cliente["sobrenome"] ." | CFP:". $row_cliente["cpf"] ?> </option>
                                                        <?php
                                                    }
                                                }else if($tipo_user == 3){
                                                    ?>
                                                    <option value= "<?php echo $_SESSION["id"]; ?>"> <?php echo $_SESSION["nome"] ." ". $_SESSION["sobrenome"] ." | CFP:". $_SESSION["cpf"] ?> </option>
                                                <?php
                                                }
                                            ?>
                                    </select>
                                    <?php
                                    
                                    ?>
                                </div>
                            </div>
                            <div class="col-2">
                                <label class="label">Foto de Perfil</label>
                                <label for="file">Selecione o arquivo</label>
                                <input type="file" name="file" id="file" required>
                                <br>
                            </div>
                        </div>

                        <div class="p-t-15">
                            <button class="btn btn--radius-2 btn--blue" type="submit" value="Salvar" name="bntCadastrar">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function mascara(i,t){

            var v = i.value;

            if(isNaN(v[v.length-1])){
                i.value = v.substring(0, v.length-1);
                return;
            }

            if(t == "cpf"){
                i.setAttribute("maxlength", "14");
            if (v.length == 3 || v.length == 7) i.value += ".";
            if (v.length == 11) i.value += "-";
            }
            if(t == "tel"){
                if(v[0] == 9){
                     i.setAttribute("maxlength", "10");
                    if (v.length == 5) i.value += "-";
                    }else{
                    i.setAttribute("maxlength", "9");
                if (v.length == 4) i.value += "-";
                    }

            }
        }
    </script>

<?php
        }else{
            echo "Usuário não autenticado.";
        }
    ?>
</body>

</html>