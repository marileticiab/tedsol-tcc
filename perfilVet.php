<?php
session_start();
include_once('conexao.php');
?>

<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
        @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

</head>

<body className='snippet-body'>

    <?php
        if(isset($_SESSION["usuario"])){
            require_once("menuVet.php");
    ?>

        <!--Container Main start-->

        <div class="container">

            <h1 class="title-model">Perfil</h1>
            <br>
            <br>
           
            <?php
                $cpf = $_SESSION['cpf'];
                $sql = "SELECT * FROM veterinario WHERE cpf = '$cpf'";
                $dados = $conn->query($sql);
                $vet = $dados->fetch_assoc();
            ?>

            <h6>Foto de Perfil</h6>
            <div class="foto-perfil" id="foto-perfil">
                <img src="uploads/<?php echo $vet['foto_perfil']?>" width="200px">
            </div>
            <a href="editarImg.php?id=<?php echo $vet['id']?>&tipo_usuario=<?php echo $vet['tipo_usuario']?>" value="update"><i class="bi bi-pencil-square"></i> Adicionar/Editar Foto</a>
            <br>
            <br>
            <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Data de Nasc.</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <?php echo $vet['nome'] ." ". $_SESSION['sobrenome']; ?> </td>
                        <td> <?php echo $vet['cpf']; ?> </td>
                        <td> <?php echo $vet['datanasc']; ?> </td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>Formação</th>
                        <th>Facul. de Grad.</th>
                        <th>CRMV</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <?php echo $vet['formacao']; ?></td>
                        <td> <?php echo $vet['faculdade']; ?></td>
                        <td> <?php echo $vet['crmv']; ?></td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>Telefone</th>
                        <th colspan="2">E-mail</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $vet['telefone']; ?></td>
                        <td colspan="2"><?php echo $vet['email']; ?></td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>Endereço</th>
                        <th>Estado</th>
                        <th>País</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $vet['rua'] .', '. $vet['num'] .', '.  $vet['bairro'] .', '.  $vet['cidade']; ?></td>
                        <td><?php echo $vet['estado']; ?></td>
                        <td><?php echo $vet['pais']; ?></td>
                    </tr>
                </tbody>
            </table>

            <br>
            <br>
            <a  class="bnt-add" href="editarVet.php?id=<?php echo  $vet['id'] ?>"><i class="bi bi-pencil-square"></i></a>
            <?php
                $verifica = 1;
            ?>
            <a class="bnt-add" onclick="confirmaExclusao('<?php echo $vet['id'];?>', '<?php echo $verifica;?>')"><i class="bi bi-trash3"></i></a>

            <a  class="bnt-add" href="editarSenha.php?id=<?php echo $vet['id'] ?>&tipo_usuario=<?php echo $vet['tipo_usuario'] ?>">Editar Senha<i class="bi bi-pencil-square"></i></a>
        </div>

        <!--Container Main end-->
        <script>
            function confirmaExclusao(id, verifica){
                if(window.confirm("Deseja realmente excluir seu perfil?")){
                    window.location = "excluirPerfilVet.php?id=" +  id + "&verifica=" + verifica;
                }
            }
        </script>

        <?php
        }else{
            echo "Usuário não autenticado.";
        }
        ?>
    </body>

</html>