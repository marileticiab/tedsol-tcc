<?php
    include_once("conexao.php");

    if(isset($_POST["senha"])){
       
        $id = $_GET["id"];
        $tipo_usuario = $_GET["tipo_usuario"];
        $senha = $_POST["senha"];

        if($tipo_usuario == 2 || $tipo_usuario == 1){
            $sql_update = "UPDATE veterinario SET senha = '$senha' WHERE id = '$id'";
        }else{
            $sql_update = "UPDATE clientes SET senha = '$senha' WHERE id = '$id'";
        }

        if($conn->query($sql_update) === TRUE){?>
            <script>
            alert("Atualizado com sucesso!");
            history.go(-2);          
            </script>
            <?php
        }else{?>
            <script>
            alert("Erro ao atualizar.");
            window.history.back();
            </script>
        <?php
        }
    }
?>

<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
    <link href="img/logotipo2.png" rel="icon">

    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Editar - TedSol</title>

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">

                    <h2 class="title">Editar Senha</h2>

                    <form method="POST" action="editarSenha.php?id=<?php echo $_GET['id']?>&tipo_usuario=<?php echo $_GET['tipo_usuario']?>">
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nova Senha</label>
                                    <input required placeholder = "*" class="input--style-4" type="text" name="senha">
                                    <br>
                                </div>
                            </div>
                        </div>

                        <button type="submit" name="submit" value="Enviar" class="btn btn--radius-2 btn--blue">OK</button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>