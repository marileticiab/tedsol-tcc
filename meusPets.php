<?php
include_once('conexao.php');
session_start();
?>

<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
        @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

</head>

<body className='snippet-body'>

<?php
    if(isset($_SESSION["usuario"])){
        require_once("menuCliente.php");
?>
        <!--Container Main start-->

        <div class="container">
            <h1 class="title-model">Pets</h1>

            <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Idade</th>
                        <th>Raça</th>
                        <th>Peso</th>
                        <th>Editar</th>
                        <th>Excluir</th>
                        <th>Perfil do Pet</th>
                        <th>Cartão de Vacina / Histórico</th>
                    </tr>
                </thead>

              <?php
                        $cliente = $_SESSION['id'];
                        $sql = "SELECT * FROM pet WHERE id_cliente = '$cliente' ORDER BY nome";
                        $dados_pet = $conn->query($sql);

                        if($dados_pet->num_rows > 0){

                            while($exibir = $dados_pet->fetch_assoc()){
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'];?></td>
                        <td><?php echo $exibir['datanasc'];?></td>
                        <td><?php echo $exibir['raca'] ;?></td>
                        <td><?php echo $exibir['peso'] ;?> Kg</td>
                        <td><a href="editarPet.php?id=<?php echo $exibir['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a style = "font-color: blue" onclick="confirmaExclusao('<?php echo $exibir['id'];?>')"><i class="bi bi-trash3"></i></a></td>
                        <td><a class="bnt-add" href="perfilPet.php?id=<?php echo $exibir['id'];?>"><i class="bi bi-arrow-up-right-square-fill"></a></td>
                        <td><a  class="bnt-add" href="cartaoVacinaPet.php?id=<?php echo $exibir['id'];?>"><i class="bi bi-postcard"></i></a></td>
                    </tr>
                </tbody>

                <?php
                    }
                }/*else{
                    echo "Não há pets cadastrados.";
                }*/
            }
    
                ?>
            </table>

            <br>
            
            <button class="bnt-add" id="bnt2">Adicionar Pet</button>
        </div>

        <!--Container Main end-->

        <script>
            function confirmaExclusao(id){

            if(window.confirm("Deseja realmente excluir esse perfil?")){
                window.location.href = "excluirPet.php?id=" +  id;
            }
            }

            document.getElementById("bnt2").addEventListener("click", function() {
            window.location.href = "cadastroPet.php";
            });
        </script>

    <?php
        /*}else{
            echo "Usuário não autenticado.";
        }*/
    ?>
    </body>

</html>