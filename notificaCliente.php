<?php
require_once('src/PHPMailer.php');
require_once('src/SMTP.php');
require_once('src/Exception.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include('conexao.php');
$id_agenda = $_GET["id"];
$info = $_GET["info"];

$sql_buscaaegenda = "SELECT * FROM agendamento WHERE id = '$id_agenda'";
$buscaagenda = $conn->query($sql_buscaaegenda);

if($buscaagenda->num_rows == 1){
    $resultado = $buscaagenda->fetch_assoc();
    $id_pet = $resultado['id_pet'];
    $id_vet = $resultado['id_veterinario'];
    $id_tipo = $resultado['tipo'];
    $horario = $resultado['horario'];
    $data = $resultado['dia'];

    //Vet
    $sql_buscaavet = "SELECT nome, sobrenome FROM veterinario WHERE id = '$id_vet'";
    $buscavet = $conn->query($sql_buscaavet);
    $resultado4 = $buscavet->fetch_assoc();
    $nome_vet = $resultado4['nome'];
    $sobrenome_vet = $resultado4['sobrenome'];
   
    //Tipo
    $sql_buscatipo = "SELECT * FROM tipo_agendamento WHERE id = '$id_tipo'";
    $buscatipo = $conn->query($sql_buscatipo);
    $resultado5 = $buscatipo->fetch_assoc();
    $tipo = $resultado5['tipo'];

    $sql_buscapet = "SELECT id_cliente, nome FROM pet WHERE id = '$id_pet'";
    $buscapet = $conn->query($sql_buscapet);

    if($buscapet->num_rows > 0){
        $resultado2 = $buscapet->fetch_assoc();
        $id_cliente = $resultado2['id_cliente'];
        $nome_pet = $resultado2['nome'];

        $sql_buscacliente = "SELECT email, nome FROM clientes WHERE id = '$id_cliente'";
        $buscacliente = $conn->query($sql_buscacliente);

        if($buscacliente->num_rows > 0){
            $resultado3 = $buscacliente->fetch_assoc();
            $email_cliente = $resultado3['email'];
            $nome_cliente = $resultado3['nome'];
           
            $mail = new PHPMailer(true);

            try{

                $mail->SMTPDebug = SMTP::DEBUG_SERVER;
                $mail->isSMTP();
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'tedsol.tcc@gmail.com';
                $mail->Password = 'lvrwhatsenulduhp';
                $mail->Port = 465; //587;

                $mail->setFrom('tedsol.tcc@gmail.com');
                $mail->addAddress($email_cliente);
                $mail->SMTPSecure = 'ssl';
                //$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;

                $mail->isHTML(true);
                $mail->Subject = 'Seu agendamento é hoje!';
                $mail->Body = 'Olá cliente ' . $nome_cliente .'! Aqui é a <strong>Tedsol</strong>. <br> Informamos que o seu agendamento está chegando! <br><br>  <strong>Detalhes:</strong> <br> PET: ' . $nome_pet . ' <br> TIPO: ' . $tipo . '<br> DATA:' . $data .'<br> HORÁRIO: '. $horario .' <br> VETERINÁRIA(O): '. $nome_vet . ' ' . $sobrenome_vet . '.<br><br> Certifique-se dos horários e mais detalhes no site. <br> Atenciosamente, TS.';
                //$mail->AltBody = 'Chegou o email teste da TEDSOL';

                if($mail->send()){
                    echo 'Enviado com sucesso!';
                    ?>
                    <script>
                    alert("Enviado com sucesso!");
                    history.go(-1);
                    </script>
                    <?php

                    //UPDADE
                    $sql_update = "UPDATE agendamento SET notificado = '1' WHERE id = '$id_agenda'";
                    $conn->query($sql_update);

                }else{
                    echo 'Erro ao enviar!';
                    ?>
                    <script>
                    alert("Erro ao enviar!");
                    history.go(-1);
                    </script>
                    <?php
                }

            }catch(Exception $e){
                echo "Erro ao enviar mensagem: {$mail->ErrorInfo}";
            }
        }
    }  
}
?>