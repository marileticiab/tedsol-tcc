<?php
include_once("conexao.php");
session_start();

$pesquisa = $_POST["pesquisa"];

    //iniciando sessão
    if(isset($_SESSION["id"])){
        //session_start();
               

                        $sql_2 = "SELECT *
                        FROM clientes 
                        WHERE nome like '%$pesquisa%'
                        or sobrenome like '%$pesquisa%'
                        ORDER BY nome";

                        $dados_clientes = $conn->query($sql_2);

                        if($dados_clientes->num_rows > 0){
?>


<table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Cidade</th>
                        <th>Contatos</th>
                        <th>Pets Relacionados</th>
                        <th>Editar</th>
                        <th>Excluir</th>
                    </tr>
                </thead>

                <?php
                    while($exibir = $dados_clientes->fetch_assoc()){
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'] ." ". $exibir['sobrenome'];?></td>
                        <td><?php echo $exibir['cpf'];?></td>
                        <td><?php echo $exibir['cidade'] ;?></td>
                        <td><?php echo $exibir['email'] ;?> <br> <?php echo $exibir['telefone'] ;?></td>
                        <td><a href="showPets.php?id=<?php echo $exibir['id'];?>"><i class="bi bi-three-dots"></a></td>
                        <td><a href="editarCliente.php?id=<?php echo $exibir['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a style = "font-color: blue" onclick="confirmaExclusao('<?php echo $exibir['id'];?>')"><i class="bi bi-trash3"></i></a></td>
                    </tr>
                </tbody>

                <?php
                    }
                }else{
                    echo "Não há clientes cadastrados.";
                }
            } 
        
                ?>

        </table>