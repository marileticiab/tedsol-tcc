<?php

include_once("conexao.php");

if(isset($_POST["senha"])){

    $nome = $_POST["nome"];
    $sobrenome = $_POST["sobrenome"];
    $cpf = $_POST["cpf"];
    $datanascimento = $_POST["datanascimento"];
    $email = $_POST["email"];
    $telefone = $_POST["telefone"];
    $cep = $_POST["cep"];
    $complemento = $_POST["complemento"];
    $rua = $_POST["rua"];
    $numero = $_POST["numero"];
    $bairro = $_POST["bairro"];
    $cidade = $_POST["cidade"];
    $estado = $_POST["estado"];
    $pais = $_POST["pais"];
    $formacao = $_POST["formacao"];
    $faculdade = $_POST["faculdade"];
    $crmv = $_POST["crmv"];
    $senha = $_POST["senha"];
    $tipo = $_POST["opcao"];

    $file = $_FILES["file"];
    
        $folder = "uploads";

        $permite = array("tif", "jpg", "png", "pdf", "jfif", "webp");
        $maxSize = 1024 * 1024 * 5;

        $msg = array();
        $erroMsg = array( 1 => "O arquivo é maior do que o permitido." ,
                            2 => "Não foi possível fazer o upload.");
        
        $name = $file["name"];
        $type = $file["type"];    
        $size = $file["size"];  
        $error = $file["error"];  
        $tmp = $file["tmp_name"]; 
        
        $extensao = @end(explode(".", $name));  
    
        $novoNome = rand() . ".$extensao";

        if($error != 0){
            $msg[] = "<b>$name</b>" . $erroMsg[$error];

        }else if(!in_array($extensao, $permite)){
            $msg[] = "<b>$name</b>" . "Tipo de arquivo não suportado.";
        
        }else if($size > $maxSize){
            $msg[] = "<b>$name</b>" . "Tamanh o maior que o permitido.";
        
        }else{
            if(move_uploaded_file($tmp, $folder."/".$novoNome)){
                  
                $sql = "INSERT INTO veterinario (nome, sobrenome, cpf, datanasc, email, telefone, cep, complemento, rua, num, bairro, cidade, estado, pais, formacao, faculdade, crmv, senha, foto_perfil, tipo_usuario) 
                VALUES('$nome', '$sobrenome', '$cpf', '$datanascimento', '$email', '$telefone', '$cep', '$complemento','$rua', $numero, '$bairro', '$cidade', '$estado', '$pais', '$formacao', '$faculdade', $crmv, '$senha','$novoNome', '$tipo')";

                if($conn->query($sql) === TRUE){
                    ?>
                    <script>
                    alert("Cadastro realizado com sucesso!");
                    history.go(-2);
                    </script>
                <?php
                }else{
                ?>
                    <script>
                    alert("Falha ao cadastrar!");
                    window.history.back();
                    </script>
<?php
        }
    }
}
}

?>