<?php
include_once("conexao.php");
session_start();

$pesquisa = $_POST["pesquisa"];

    //iniciando sessão
    if(isset($_SESSION["id"])){
        $tipo_user = $_SESSION['tipo_usuario'];

                        $sql_2 = "SELECT *
                        FROM veterinario 
                        WHERE nome like '%$pesquisa%'
                        or sobrenome like '%$pesquisa%'
                        ORDER BY nome";

                        $dados_vet = $conn->query($sql_2);

                        if($dados_vet->num_rows > 0){
?>
 <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                    </tr>
                </thead>

                <?php
                    while($exibir = $dados_vet->fetch_assoc()){
                        $telefone = $exibir['telefone'];
                        $telefone = preg_replace('/[^0-9]/', '', $telefone);
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'] ." ". $exibir['sobrenome'];?></td>
                        <td><a href="https://wa.me/55<?php echo $telefone?>" target="_blank"> <i class="bi bi-whatsapp"></i> </a></td>
                        <td><?php echo $exibir['email']?></td>
                    </tr>
                </tbody>

                <?php
                    }
                }else{
                    echo "Não há veterinários cadastrados.";
                }
            }
                ?>
            </table>