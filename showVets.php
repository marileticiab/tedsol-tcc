<?php
include_once('conexao.php');
session_start();
?>

<!doctype html>
<html lang="pt">

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
    @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <script>
    $(document).ready(function() {
        $("#form-pesquisa").submit(function(evento) {

            evento.preventDefault();
            let pesquisa = $("#pesquisa").val();
            let dados = {
                pesquisa: pesquisa
            }

            //alert(pesquisa);

            $.post("buscaVet.php", dados, function(retorna) {
                $(".resultados").html(retorna);
            });
        });
    });
    </script>
</head>

<body className='snippet-body'>

    <?php
    if(isset($_SESSION["usuario"])){
        require_once("menuVet.php");
        $tipo_user = $_SESSION['tipo_usuario'];
    ?>
    <!--Container Main start-->

    <div class="container">
        <h1 class="title-model">Veterinários</h1>
        <!--Barra de pesquisa start-->
        <form name="form-pesquisa" id="form-pesquisa" method="POST">
            <div class="row row-space">
                <div class="col-2">
                    <div class="input-group">
                        <input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquise nomes...">
                        <br>
                        <input class="bnt-add" type="submit" name="enviar" value="Pesquisar">
                    </div>
                </div>
            </div>
        </form>
        <br>
        <div class="resultados">
            
            <?php
                $sql = "SELECT * FROM veterinario ORDER BY nome";
                $dados_vet = $conn->query($sql);

                if($dados_vet->num_rows > 0){
                    ?>

            <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>CRMV</th>
                        <th>Formação</th>
                        <th>Contatos</th>

                        <?php if($tipo_user == 1){?>
                        <th>Editar</th>
                        <th>Excluir</th>
                        <?php } ?>

                    </tr>
                </thead>

                <?php
                    while($exibir = $dados_vet->fetch_assoc()){
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $exibir['nome'] ." ". $exibir['sobrenome'];?></td>
                        <td><?php echo $exibir['cpf'];?></td>
                        <td><?php echo $exibir['crmv'] ;?></td>
                        <td><?php echo $exibir['formacao'] ;?></td>
                        <td><?php echo $exibir['email'] ;?> <br> <?php echo $exibir['telefone'] ;?></td>
                        <?php if($tipo_user == 1){?>
                            
                        <td><a href="editarVet.php?id=<?php echo $exibir['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                        <?php
                            $verifica = 2;
                        ?>
                        <td><a style="font-color: blue"
                                onclick="confirmaExclusao('<?php echo $exibir['id'];?>', '<?php echo $verifica;?>')"><i class="bi bi-trash3"></i></a></td>

                                <?php } ?>
                    </tr>
                </tbody>

                <?php
                    }
                }else{
                    echo "Não há veterinários cadastrados.";
                }
            }
                ?>

            </table>
        </div>

        <!--Barra de pesquisa end-->
        <br>
        <br>
        
        <?php if($tipo_user == 1){?>
        <button class="bnt-add" id="bnt">Adicionar Veterinário</button>
        <?php } ?>
    </div>

    <!--Container Main end-->

        <script>
            function confirmaExclusao(id, verifica){
                if(window.confirm("Deseja realmente excluir esse perfil?")){
                    window.location = "excluirPerfilVet.php?id=" +  id + "&verifica=" + verifica;
                }
            }

    document.getElementById("bnt").addEventListener("click", function() {
        window.location.href = "cadastroVet.php";
    });
    </script>

    <?php
        /*}else{
            echo "Usuário não autenticado.";
        }*/
    ?>



</body>

</html>