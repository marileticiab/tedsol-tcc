<?php
session_start();
include_once('conexao.php');
?>

<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
    @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

</head>

<body className='snippet-body'>

    <?php
    //iniciando sessão
    if(isset($_SESSION["usuario"])){
        require_once("menuCliente.php");
       
?>
    <!--Container Main start-->

    <div class="container">

        <header class="head-info">
            <h1 class="title-model">Bem-vindo(a), cliente <?php echo $_SESSION['nome'];?>! </h1>
            <div class="image-container">
                <img src="img/sobre.png" alt="">
            </div>
            <br>
            <br>

            <nav>
                <ul>
                    <li><a href="perfilCliente.php">Perfil</a></li>
                    <li><a href="agenda.php">Agendamento</a></li>
                    <li><a href="meusPets.php">Meus Pets</a></li>
                    <li><a href="mensagem.php">Mensagens</a></li>
                </ul>
            </nav>
            <br>
            <br>
            <div class="image-container-2">
                <img src="img/patinhas.png" alt="">
            </div>

        </header>
        <br>
        <br><br><br>
        <main class="body-inicio">
        <h2>O que gostaria de acessar hoje?</h2>
        <!--consultas marcadas-->
        <?php
                  // Get current date and time in Brasilia timezone
                  date_default_timezone_set('America/Sao_Paulo');
                  $currentDateAndTime = new DateTime();
              
                  // Get date components
                  $day = $currentDateAndTime->format('d');
                  $month = $currentDateAndTime->format('m');
                  $year = $currentDateAndTime->format('Y');
              
                  // Get time components
                  $hours = $currentDateAndTime->format('H');
                  $minutes = $currentDateAndTime->format('i');
                  $seconds = $currentDateAndTime->format('s');
              
                  // Output the current date and time in Brasilia timezone
                  //echo "Data e Hora atual em Brasília: " . $currentDateAndTime->format('Y-m-d H:i:s') . "\n";


                $cliente =  $_SESSION['id'];
                
                $sql_pets = "SELECT id, nome FROM pet WHERE id_cliente = '$cliente'";
                $seus_pets = array();
                $seus_pets = $conn->query($sql_pets);
                //echo $seus_pets[1];
                if($seus_pets->num_rows > 0){
                   
                    while($dados_pet = $seus_pets->fetch_assoc()){
                    $dataatual=$currentDateAndTime->format('Y-m-d');
                    $sql = "SELECT * FROM agendamento WHERE id_pet = " . $dados_pet['id']. " AND dia >= '" .$dataatual. "' ORDER BY dia";

                    //echo $sql;
                    $dados_agendamento = $conn->query($sql);
                    if($dados_agendamento->num_rows > 0){
                        
                    ?>
                    <br><br><br><br>
        <h3>Seus Agendamentos</h3>
        <table class="styled-table">
            <thead>
                <th colspan="5">Agendamento do pet : <?php echo $dados_pet["nome"]?></th>
            </thead>
            <thead>
                <th>Dia</th>
                <th>Horário</th>
                <th>Tipo</th>
                <th>Situação</th>
                <th>Cancelar</th>
            </thead>
            <?php
                        while($exibir = $dados_agendamento->fetch_assoc()){
                            
                ?>

            <tbody>
                <tr>
                    <td><?php echo $exibir['dia'];?></td>
                    <td><?php echo $exibir['horario'];?></td>
                    <?php
                            $tipo= $exibir['tipo'];
                            $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                            $dadostipo= $conn->query($sqltipo);
                            $nometipo= $dadostipo->fetch_assoc();
                        ?>
                    <td><?php echo $nometipo['tipo'];?></td>

                    <?php
                    $situacao = $exibir['situacao'];
                    $sql_situacao = "SELECT * FROM situacao_agendamento WHERE id = '$situacao'";
                    $dados_situacao= $conn->query($sql_situacao);
                    $situacao_resultado= $dados_situacao->fetch_assoc();
                    ?>
                    <td <?php if($exibir['situacao']==1){?>
                    style = "color: green"
                    <?php }else if($exibir['situacao']==2){?>
                    style = "color: orange"
                    <?php }else{?>
                    style = "color: red" 
                    <?php } ?>
                    ><?php echo $situacao_resultado['situacao'];?></td>
        
                    <?php
                            $pet= $exibir['id_pet'];
                            $sqlpet= "SELECT * FROM pet WHERE id = '$pet'";
                            $dadospet= $conn->query($sqlpet);
                            $nomepet= $dadospet->fetch_assoc();
                        ?>
                    <td><a class="bnt-add" onclick="confirmaCancelamento('<?php echo $exibir['id'];?>')"><i class="bi bi-calendar2-x-fill"></i></a></td>
                </tr>
            </tbody>

            <?php
                    }
                }
                    
                    }
                }else{
                    echo "Não há agendamentos cadastrados.";
                }
      

            
                }
                ?>
        </table>
            </main>
        <br><br><br><br>
        <footer>
            <p>@tedsoltcc</p>
        </footer>
    </div>

    <!--Container Main end-->

    <?php
        /*}else{
            echo "Usuário não autenticado.";
        }*/
    ?>

    <script>
    function confirmaCancelamento(id){
        if(window.confirm("Deseja realmente cancelar esse agendamento?")){
            window.location.href = "cancelaAgendamento.php?id=" +  id;
        }
    }

    function getCurrentDateAndTime() {
        let currentDateAndTime = new Date();
        let hours = currentDateAndTime.getHours();
        let minutes = currentDateAndTime.getMinutes();
        let seconds = currentDateAndTime.getSeconds();
        let day = currentDateAndTime.getDate();
        let month = currentDateAndTime.getMonth() + 1;
        let year = currentDateAndTime.getFullYear();

        return new Date(year, month - 1, day, hours, minutes, seconds);
    }
    let currentDateAndTime = getCurrentDateAndTime();
    console.log("Data e Hora atual: " + currentDateAndTime);
    </script>
</body>

</html>