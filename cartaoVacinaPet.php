<?php
include_once('conexao.php');
session_start();
?>

<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
        @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

</head>

<body className='snippet-body'>

    <body id="body-pd">
        
    <?php
    if(isset($_SESSION["usuario"])){
        $usuario = $_SESSION["tipo_usuario"];

        if($usuario == 1 || $usuario == 2){
            require_once("menuVet.php");
        }else if($usuario == 3){
            require_once("menuCliente.php");
        }
    ?>
        <!--Container Main start-->
       
            <div class="container">

            <?php
                $idPet = $_GET["id"];
                $sqlPet = "SELECT * FROM pet WHERE id = $idPet";

                $consultaPet = $conn->query($sqlPet);
                $dadosPet = $consultaPet->fetch_assoc();

            ?>
            <h1 class="title-model">Cartão de Vacinação</h1>
            <br>

            <h6>Foto de Perfil</h6>
            <div class="foto-perfil" id="foto-perfil">
                <img src="uploads/<?php echo $dadosPet['foto_perfil'] ?>" height="auto" width="200">
            </div>

            <table class="styled-table">
                <thead>
                    <tr>
                        <th colspan="4"><h5><u>Informações do Pet</u></h5></th>                
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th>Nome do Pet</th>
                        <th>Raça</th>
                        <th>Espécie</th>
                        <th>Data de Nasc.</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $dadosPet['nome']; ?></td>
                        <td><?php echo $dadosPet['raca']; ?></td>
                        <td><?php echo $dadosPet['especie']; ?></td>
                        <td><?php echo $dadosPet['datanasc']; ?></td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th colspan="4"><h5><u>Controle de Vacinação</u></h5></th>
                    </tr>
                </thead>
               
                <thead>
                    <tr>
                        <th>Data Aplicação</th>
                        <th>Vacina</th>
                        <th>Data Reforço</th>
                        <th>Validação do Veterinário</th>
                    </tr>
                </thead>
                <?php
                    $sqlvac = "SELECT * FROM cartao_vacina WHERE id_pet = $idPet";
                    $consultavac = $conn->query($sqlvac);    
                    if($consultavac->num_rows > 0){
                        
                        while($dadosvac = $consultavac->fetch_assoc()){
                            if($dadosvac['tipo'] == 1){
                ?>
                <tbody>
                    <tr>
                        <td><?php echo $dadosvac['data_aplicacao'];?></td>
                        <td><?php echo $dadosvac['nome'];?></td>
                        <td><?php echo $dadosvac['data_reforco'];?></td>
                        <td>
                        <?php if($usuario == 1 || $usuario == 2){?>    
                        <form method="POST" action="validacaoVet.php?id=<?php echo $dadosvac['id']?>">
                            <button class="bnt-add" onclick="checkInput('myCheckbox')"> Clique para validar</button>
                        </form>
                        <?php }
                            $validacao_vet = $dadosvac['validacao_vet'];
                            $sql_validacao= "SELECT * FROM validacao_vet WHERE id = '$validacao_vet'";
                            $dados_validacao= $conn->query($sql_validacao);
                            $validacao_vet_resultado= $dados_validacao->fetch_assoc();
                        ?>
                           <p <?php if($validacao_vet_resultado['id'] == 2){?>
                           style="color: red"
                           <?php }else{?>
                            style="color: green" <?php }?>
                           ><?php echo $validacao_vet_resultado['validacao']?></p>
                    </td>
                    </tr>
                </tbody>
                <?php
                        }}}
                        ?>
                <thead>
                    <tr>
                        <th colspan="4"><h5><u>Controle Parasitário</u></h5></th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th>Data Aplicação</th>
                        <th>Vermífugo</th>
                        <th>Data Reforço</th>
                        <th>Validação do Veterinário</th>
                </thead>
                <?php
                $sqlvac = "SELECT * FROM cartao_vacina WHERE id_pet = $idPet";
                $consultavac = $conn->query($sqlvac);   
                    if($consultavac->num_rows > 0){
                      while($dadosvac = $consultavac->fetch_assoc()){
                         if($dadosvac['tipo']==2){
                ?>
                <tbody>
                    <tr>
                    <td><?php echo $dadosvac['data_aplicacao'];?></td>
                        <td><?php echo $dadosvac['nome'];?></td>
                        <td><?php echo $dadosvac['data_reforco'];?></td>
                        <td>
                        <?php if($usuario == 1 || $usuario == 2){?>    
                        <form method="POST" action="validacaoVet.php?id=<?php echo $dadosvac['id']?>">
                            <button class="bnt-add" onclick="checkInput('myCheckbox')"> Clique para validar</button>
                        </form>
                        <?php }
                            $validacao_vet = $dadosvac['validacao_vet'];
                            $sql_validacao= "SELECT * FROM validacao_vet WHERE id = '$validacao_vet'";
                            $dados_validacao= $conn->query($sql_validacao);
                            $validacao_vet_resultado= $dados_validacao->fetch_assoc();
                        ?>
                           <p <?php if($validacao_vet_resultado['id'] == 2){?>
                           style="color: red"
                           <?php }else{?>
                            style="color: green" <?php }?>
                           ><?php echo $validacao_vet_resultado['validacao']?></p>
                        </td>
                    </tr>
                </tbody>
                <?php
                        }
                    }
                }

                if($usuario == 1 || $usuario == 2){
                        ?>
                <tbody>
                    <tr>
                        <td colspan="4" ><a href="cadastroCartaoVacina.php?id=<?php echo $dadosPet['id']; ?>">Adicionar Vacina/Vermífugo</a></td>
                    </tr>
                </tbody>
                <?php
                }
                ?>
                <thead>
                    <tr>
                        <th colspan="4"><h5><u>Histórico de Consultas</u></h5></th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th>Data da Consulta</th>
                        <th>Tipo</th>
                        <th colspan="2">Clínica/Veterinário</th>
                </thead>
                <?php
                    $sql = "SELECT * FROM agendamento WHERE id_pet = '$idPet' ORDER BY dia";
                    $dados_agendamento = $conn->query($sql);

                    if($dados_agendamento->num_rows > 0){

                        while($exibir = $dados_agendamento->fetch_assoc()){
                ?>
                <tbody>
                    <tr>
                        <td><?php echo $exibir['dia'] ;?></td>
                        <?php
                            $tipo= $exibir['tipo'];
                            $sqltipo= "SELECT * FROM tipo_agendamento WHERE id = '$tipo'";
                            $dadostipo= $conn->query($sqltipo);
                            $nometipo= $dadostipo->fetch_assoc();
                        ?>
                        <td><?php echo $nometipo['tipo'];?></td>

                        <?php
                            $vetid=$exibir['id_veterinario'];
                            $sqlvet= "SELECT * FROM veterinario WHERE id = '$vetid'";
                            $dadosvet= $conn->query($sqlvet);
                            $nomevet= $dadosvet->fetch_assoc();
                        ?>

                        <td colspan="2"><?php echo $nomevet['nome'];?></td>
                    </tr>
                </tbody>
                <?php
                    }
                    /*}else{
                        echo "Não há agendamentos cadastrados.";
                    }*/
                    }
                }
                ?>
            </table>
            <br>
            <br>
            </div>
    
        <!--Container Main end-->
    </body>

</html>