<?php
session_start();
include_once('conexao.php');
?>

<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TedSol</title>
    <link href="img/logotipo2.png" rel="icon">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

    <!--CSS-->
    <link rel="stylesheet" href="css/style-main.css">

    <style>
        @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    </style>

    <!-- Vendor CSS Files -->
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

</head>

<body className='snippet-body'>

<?php
    //iniciando sessão
    if(isset($_SESSION["usuario"])){
        require_once("menuCliente.php");
?>
        <!--Container Main start-->

        <div class="container">
            
            <h1 class="title-model">Perfil</h1>
            <br>
            <br>
            <?php
                $cpf = $_SESSION['cpf'];
                $sql = "SELECT * FROM clientes WHERE cpf = '$cpf'";
                $dados = $conn->query($sql);
                $clientes = $dados->fetch_assoc();
            ?>

            <!--Foto de perfil-->
            <h6>Foto de Perfil</h6>
            <div class="foto-perfil" id="foto-perfil">
                <img src="uploads/<?php echo $clientes['foto_perfil'] ?>" height="auto" width="200">
                </div>
            <br>
            <br>
                <a href="editarImg.php?id=<?php echo $clientes['id']?>&tipo_usuario=<?php echo $clientes['tipo_usuario']?>" value="update"><i class="bi bi-pencil-square"></i>Adicionar/Editar Foto</a>
            <br><br>
            <table class="styled-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Data de Nasc.</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $clientes['nome'] ." ". $clientes['sobrenome']; ?></td>
                        <td><?php echo $clientes['cpf']; ?></td>
                        <td><?php echo $clientes['datanasc']; ?></td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>Telefone</th>
                        <th colspan="2">E-mail</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $clientes['telefone']; ?></td>
                        <td colspan="2"><?php echo $clientes['email']; ?></td>
                        
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>Endereço</th>
                        <th>Estado</th>
                        <th>País</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $clientes['rua'] .', '. $clientes['num'] .', '.  $clientes['bairro'] .', '.  $clientes['cidade']; ?></td>
                        <td><?php echo $clientes['estado']; ?></td>
                        <td><?php echo $clientes['pais']; ?></td>
                    </tr>
                </tbody>
            </table>

            <a class="bnt-add" href="editarCliente.php?id=<?php echo  $clientes['id'] ?>"><i class="bi bi-pencil-square"></i></a>

            <a class="bnt-add" onclick="confirmaExclusao('<?php echo $clientes['id'];?>')"><i class="bi bi-trash3"></i></a>

            <a  class="bnt-add" href="editarSenha.php?id=<?php echo $clientes['id'] ?>&tipo_usuario=<?php echo $clientes['tipo_usuario'] ?>">Editar Senha<i class="bi bi-pencil-square"></i></a>

            <!--Container Main end-->
        <script>

             function confirmaExclusao(id){

                if(window.confirm("Deseja realmente excluir seu perfil?")){
                    window.location = "excluirPerfilCliente.php?id=" +  id;
                }
            }

        </script>

    <?php
        }else{
            echo "Usuário não autenticado.";
        }
    ?>
    </body>

</html>