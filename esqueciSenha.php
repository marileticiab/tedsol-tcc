<?php

require_once('src/PHPMailer.php');
require_once('src/SMTP.php');
require_once('src/Exception.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include_once("conexao.php");
session_start();

//require './vendor/autoload.php';

if (isset($_POST['email'])) {

    //Create an instance; passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'tedsol.tcc@gmail.com';
        $mail->Password = 'lvrwhatsenulduhp';
        $mail->Port = 465; //587;
        $mail->SMTPSecure ='ssl';

        $msg = array();
        $enviou = false;
        $email =$_POST['email'];

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $msg[] = "E-mail informado é inválido. ";
        } else {
                
            $sql_code = "SELECT * from veterinario where email = '$email'";
            $sql_query = $conn->query($sql_code); //or die($conn->error);
            $qtde = $sql_query->num_rows;
            
            if($qtde==0){
                $sql_code_c  = "SELECT * from cliente where email = '$email'";
                $sql_query_c  = $conn->query($sql_code) or die($conn->error);
                $qtde = $sql_query->num_rows;
            
                if ($qtde == 0) {
                    $msg[] = "E-mail informado não existe no banco de dados.";
                }else{
                    $clt=$sql_query_c->fetch_assoc();
                    $user =3;
                   // $email= $clt['email'];
                    //é cliente
                }
            }else{
                //é veterinário
                $vet=$sql_query->fetch_assoc();
                $user=1;
               // $email= $vet['email'];
            }
        }

            if (count($msg) == 0 && $qtde > 0) {
                //$dados = $sql_query->fetch_assoc();
                $novaSenha = substr(md5(time()), 0, 6);
             //   $novaSenhaCriptografada = md5($novaSenha);
                //Recipients
                $mail->setFrom('tedsol.tcc@gmail.com', 'TedSol');
                $mail->addAddress($email);     //Add a recipient

                //Content
                $assunto = "Nova senha de acesso";
                $mensagem = "
                <html>
                <body>
                <img src='img/imagem-email.png'>
                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                 <tr>
                    <td align='center' bgcolor='#ffffff' style='padding: 20px 0 20px 0;'>
                      <img src='img\imagem-email.png'>
                    </td>
                 </tr>
                </table>
                </body>
                </html>
                ";
                
                $mensagem = "<h1 style='font-size:15px font-color: #f2d096'>Olá! Aqui é o TedSol!</h1><br><br>";
                $mensagem .= "Segue a sua nova senha de acesso: $novaSenha<br><br>";
                $mensagem .= "Caso não tenha requisitado a troca de sua senha, contate os administradores!!!";
                $mensagem .= "<br><br>Obrigado!<br>";
                $mail->isHTML(true);                                  //Set email format to HTML
                $mail->Subject = $assunto;
                $mail->Body    = $mensagem;
                //$mail->send();

                if ($mail->send()) {
                     if($user==1){
                        $sql = "UPDATE veterinario 
                            SET senha = '$novaSenha'
                            WHERE email = '$email'";
                        $sql_query = $conn->query($sql) or die($conn->error);
                        if ($sql_query) {
                            $msg[] = "Uma nova senha foi enviada ao seu e-mail.<br>Efetue o login e troque a senha!";
                            $enviou = true;
                            ?>
                                <script>
                                     alert("Uma nova senha foi enviada ao seu e-mail.<br>Efetue o login e troque a senha!");
                                    history.go(-2);
                                </script>
                            <?php
                        }
                    }else if($user==3){
                        $sql = "UPDATE cliente 
                        SET senha = '$novaSenha'
                        WHERE email = '$email'";
                        $sql_query = $conn->query($sql) or die($conn->error);
                        if ($sql_query) {
                            $msg[] = "Uma nova senha foi enviada ao seu e-mail. Efetue o login e troque a senha!";
                            $enviou = true;
                            ?>
                                <script>
                                     alert("Uma nova senha foi enviada ao seu e-mail. Efetue o login e troque a senha!");
                                    history.go(-2);
                                </script>
                            <?php
                       }
                    }
                 
                   
                }
                
    }
 
    } catch (Exception $e) {
        $msg[] = "Erro ao enviar o e-mail. Erro PHPMailer: {$mail->ErrorInfo}";
        ?>
        <script>
            alert("Erro ao enviar o e-mail!");
            history.go(-2);
        </script>
        <?php
    }
}

?>

<!DOCTYPE html>
<html lang="pt">

<head>

    <!-- Favicons -->
    <link href="img/logotipo2.png" rel="icon">

    <!-- Required meta tags-->
    <meta charset="UTF-8">

    <!-- Title Page-->
    <title>Esqueci minha senha</title>

    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor-cadastro/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor-cadastro/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/style-cadastro.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h1 class="title">Opa! Esqueceu sua senha?!</h1>
                    <h4 style="background-color: white;">Para recuperar sua senha, informe seu email, e te mandaremos sua nova senha! Por favor, lembre de trocar sua senha novamente após logar!</h4>
                    <br>
                    <form  method="POST">
                        
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                <div class="row row-space">
                        </div>
                        <br>
                        <div class="row row-space">
                            <div class="col-2">
                            <form action="" method="post" name="frmRecuperarSenha">
                            <!-- Email input -->
                                <div class="input-group">
                                    <label class="label" for="txtEmail">E-mail</label>
                                    <br>                                 
                                    <input type="text" id="email" name="email" class="input--style-4" autofocus required placeholder="Informe o e-mail" />
                                    <br>
                                </div>
                                <button class="btn btn--radius-2 btn--blue" type="submit" value="Salvar" name="bntConcluido">Enviar</button>                                
                                  
                                    <div class="p-t-15">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    const handlePhone = (event) => {
  let input = event.target
  input.value = phoneMask(input.value)
}

const phoneMask = (value) => {
  if (!value) return ""
  value = value.replace(/\D/g,'')
  value = value.replace(/(\d{2})(\d)/,"($1) $2")
  value = value.replace(/(\d)(\d{4})$/,"$1-$2")
  return value
}
</script>
</html>